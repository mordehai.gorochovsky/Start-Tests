import inspect
import signal
import pytest
from datetime import datetime
from os import environ, kill
from subprocess import check_output, check_call, Popen
from log_utilities import HtmlLog ,Steps ,print_paths_on_test_failure ,html_code, html_names
import time
import settings
import config
from pathlib import Path
import start_interface as interface
import start_utilities as utility
import os
html_log = HtmlLog()
steps = Steps()
steps.step_init(0)
#TODO before each session -> Settings -> Setup Widget -> Widget Allignment->Custom(Default)
test = interface.test#settings.test
test_adb = settings.adb_functions
device_obj = settings.device_obj
device_id = settings.device_id
home_dir = Path(environ["HOME_DIRECTORY"])
device_folder_path = Path(os.environ.get("HOME_DIRECTORY"))
videos_folder = config.videos_folder
device_video_path = "/sdcard/ScreenVideo1.mp4"
environ["RAV_CURRENT_RUNNING_TEST"] = "None"
environ["RAV_TEST_STATUS"] = "1"
environ["RAV_BLACK_WALLPAPPER"] = "1"
#print_success = True
print("videos folder", videos_folder)
#test.write_session_header()

class AdbCollectLogFromAndroid:
    def __init__(self, serial_number, device_folder_path, file_name):
        self.device_id = serial_number
        self.device_folder_path = device_folder_path
        self.file_name = file_name
        self.process = None

    def start_log(self):
        check_call(['adb', '-s', self.device_id, 'logcat', '-c'])
        with open(str(self.device_folder_path / "{}.log".format(
                self.file_name)), 'wb') as log:
            self.process = Popen(["adb", "-s", device_id, "logcat", "-vthreadtime"],
                                 stdout=log)

    def stop_log(self):
        self.process.terminate()


@pytest.fixture(scope="session", autouse=True)
def acquire_icons_coordinates() -> None:
    print("in pytest FIXTURE acquire_icons_coordinates")
    do_at_session_start()
    #
    # html_log.h_log(message="link in ravtech offices: ",link="file://" + str(videos_folder /
    #                                                    "acquiring_coordinates.mp4"))
    yield
    reverse_all_changes_on_device()


def do_at_session_start_without_coordinates_acquiring():
    print("in do_at_session_start_without_coordinates_acquiring")
    try:
        test_adb.uninstall_or_clear_cach()
        time.sleep(1)
    except Exception as e:
        print(e)

    try:
        interface.change_to_english(logging=False)
        time.sleep(2)
    except Exception as e:
        print(e)

    try:
        interface.reset_google_ads_id()
        time.sleep(5)
        interface.press_back_n_times(4)
    except Exception as e:
        print(e)

    try:
        test_adb.install_start_anyway()
    except Exception as e:
        print(e)

    try:
        interface.open_start_first_time()
    except Exception as e:
        print(e)

    try:
        interface.open_camera_by_swiping_to_multimedia_hub(logging=False)
        interface.allow_all_permisions()
        '''
        Important: verify no permissions methods allow permissions:
        
        interface.verify_no_permission_request_displayed_for_contacts_and_physical_location(logging=False)
        interface.verify_no_permission_request_for_file_access(logging=False)
                 '''
    except Exception as e:
        print(e)

    try:
        time.sleep(10)
        test.write_session_header()
        test.open_start_simple(logging=False)
        time.sleep(10)
        device_obj.screen.on()
        interface.skip_welcome_if_needed(logging=False)
        test.allow_permissions_if_needed()
    except Exception as e:
        print(e)
        #utility.if_questionary_skip()
        #interface.skip_rating()

    try:
        disable_clock_and_date_widgets_if_needed()
        time.sleep(1)
    except Exception as e:
        print(e)
    if True:#device_id in config.change_to_standard_theme:
        change_to_standart_theme()


def change_to_standart_theme():
    print("in change_to_standart_theme")
    try:
        interface.swipe_left_by_menu_icon(logging=False)
        interface.test.click_on_first_theme_in_menu_screen()
        time.sleep(3)
    except Exception as e:
        print(e)


def do_at_session_start():
    print("in do_at_session_start")
    print("#############- DEVICE - ", os.environ.get('RAV_DEVICE_FOLDER'))
    # prepare something ahead of all tests
    # #todo: delete set black ...
    # interface.set_black_background_via_more_themes()
    device_obj.screen.on()
    p_id = start_recording()
    print("befor prepare device for rtesting")
    try:
        prepare_device_for_testing()
    except Exception as e:
        print("!!!!!!!111got an exception")
        print(e)
    print("before acquire coordinates")
    acquire_coordinates()
    print("after acquire coordinates")
    stop_recording(p_id, videos_folder / "acquiring_coordinates", save_anyway=True)
    html_log.h_log(message="Acquiring coordinates video: ", link=html_log.convert_message_to_path(
        str(videos_folder / "acquiring_coordinates.mp4")))


@pytest.fixture(scope="function")
def video_recording(request) -> int:
    print("in pytest FIXTURE  video_recording")
    #global print_success
    func_name = request.function.__name__
    #test.adb.install_start_apk(log=False)
    android_log_file, p_id = execute_before_scenario(func_name)
    yield p_id
    execute_after_scenario(android_log_file, func_name, p_id)
    return -1


def execute_after_scenario(android_log_file, func_name, p_id):
    print("teardown video_recording, pid={}, function={}, duration={}".format(p_id, func_name, datetime.now()))
    android_log_file.stop_log()
    print("DEBUG: steps.get_step_counter {} environ.get(RAV_TEST_STATUS)= {}".format(
        steps.get_step_counter(), environ.get("RAV_TEST_STATUS")))
    if os.environ.get("RAV_BEHAVE") in "TRUE":
        func_name = strip_brackets(func_name)
    in_case_of_internal_error(func_name)
    save_anyway = True if environ.get("RAV_IS_DEBUG") == "true" else False
    print("save_anyway = ", save_anyway)
    stop_recording(p_id, videos_folder / func_name,save_anyway=save_anyway)
    finish_test_output_table()
    test.bring_device_to_original_state()
    in_case_wallpaper_was_changed()


def execute_before_scenario(func_name):
    print("in execute_before_scenario")
    device_obj.screen.on()
    device_obj.press.home()
    time.sleep(2)
    p_id = start_recording()
    android_log_file = start_android_logging(strip_brackets(func_name))
    start_new_table(func_name)
    test.device_obj.screen.on()
    return android_log_file, p_id


def reverse_all_changes_on_device():
    in_case_wallpaper_was_changed()
    enable_clock_and_date_widgets_if_needed()


def acquire_coordinates():
    print("in acquire coordinates")
    test.crop_all_hub_icons(place="crop_hub_icon")
    #Deprecated by Mordechai 24.1.18 test.get_hubs_coordinates()
    device_status = environ.get("DEVICE_STATUS")
    coordinates_file_valid = test.verify_coordinate_file_valid()
    print("DEBUG: environ.get(RAV_IS_DEBUG) = ", environ.get("RAV_IS_DEBUG"))
    if environ.get("RAV_IS_DEBUG") == "true":
        print("DEBUG MODE: EXISTING DEVICE COORDINATES WILL BE INITIALIZED FROM THE FILE")
        test.initialize_coordinates_from_file()
    else:
        print("devise_status: {}, cooordinates_file_valid {}".format(device_status,coordinates_file_valid))
        if device_status != "1" or not coordinates_file_valid:
            print("NEW DEVICE OR THERE IS A DIFFERENCE BETWEEN CURRENT AND PREVIOUS HUB PAGES")
            test.swipe_on_all_app_icons()
            test.get_icon_coordinates()
            test.write_to_file_all_coordinates()
        else:
            print("EXISTING DEVICE COORDINATES WILL BE INITIALIZED FROM THE FILE")
            test.initialize_coordinates_from_file()
    if not test.unlock_point:
        test.unlock_point = (int(test.screen_width*0.153),test.lock_point[1])


def prepare_device_for_testing():
    was_installed = None
    try:
        print("in prepare_device_for_testing")
        test.screen_on(logging=False)
        #TODO replace set_black_background_via_more_themes
        #interface.set_black_background_via_more_themes()
        #test_adb.close_open_apps()
        save_home_screen_xml()
        tt = time.time()
        print("before install:".format(tt))
        was_installed = test.adb.install_start_apk(is_test=False)
        tt2 = time.time()
        print("after install: {}".format(tt2))
        print("end - start: {}",tt2-tt )
    except Exception as e:
        print("prepare_device_for_testing failed")
        print(e)
    p_id = start_recording()
    #interface.set_black_background_via_more_themes()
    print("was_installed = ", was_installed)
    if was_installed:
        print("was installed in prepare_device_for_testing ")
        time.sleep(2)
        interface.verify_in_start_screen_after_instal(200)
        test.allow_permissions_if_needed()
        if was_installed == 1:
            interface.skip_tutorial()
    try:
        lock_p = test.open_start(logging=False)
        if lock_p:
            print("Lock_Point acquired successfully ")
        else:
            device_obj.screen.off()
            device_obj.screen.on()
            test.open_start(logging=False)
    except Exception as e:
        print(e)
        print("failure in lock acuisition")
    try:
        if not test.lock_point:
            test.lock_point = test.get_lock_coordinates()
    except Exception as e:
        print("second failure in lock acuisition")
        print(e)
    try:
        pass
        #disable_clock_and_date_widgets_if_needed()
    except Exception as e:
        print(e)
        print("disable_clock_and_date_widgets_if_needed failed")

    try:
        interface.fix_settings_issue()
    except Exception as e:
        print(e)
        print("fix_settings_issue failed")
    stop_recording(p_id, videos_folder / "prepare_device", save_anyway=True)


def save_home_screen_xml():
    test_adb.go_to_device_home_screen()
    device_obj.press.home()
    device_obj.dump(str(Path(os.environ.get('RAV_DEVICE_FOLDER')) / "home_screen.xml"))


#interface.skip_tutorial_if_needed()


def disable_clock_and_date_widgets_if_needed():
    print("in disable_clock_and_date_widgets_if_needed")
    print("dev_id = ",device_id)
    if device_id in config.devices_to_disable_clock_and_date_widgets:
        print("disabling")
        test.open_start_simple()
        time.sleep(4)
        interface.go_to_setup_widgets_screen()
        interface.disable_clock_widget()
        interface.disable_date_widget()
        interface.disable_battery_meter_widgget()
        device_obj.press.back()
        device_obj.press.back()
    else:
        print("disabling not needed")
        # test_debug()


def enable_clock_and_date_widgets_if_needed():
    print("enable_clock_and_date_widgets_if_needed")
    print("disabling")
    try:
        interface.go_to_setup_widgets_screen()
        interface.enable_clock_widget()
        interface.enable_date_widget()
        device_obj.press.back()
        device_obj.press.back()
    except Exception as e:
        print("Error: ",e)


def start_android_logging(test_name):
    android_log_file = AdbCollectLogFromAndroid(device_id, Path(environ.get(
        "DEVICE_FAILURE_FOLDER", str(home_dir))), test_name)
    android_log_file.start_log()
    return android_log_file


def strip_brackets(name):
    print("in strip_brackets")
    if os.environ.get("RAV_BEHAVE") in "TRUE":
        if "<" in name:
            name = name.split("<")[1]
            name = name.split(">")[0]
            name = name.split("Scenario")[1].strip()
        if "\"" in name:
            name = name.split("\"")[1]
        name = name.replace(" ", "_")

    print("scenario witout brackets ",name)
    return name


def start_new_table(func_name):
    print("-------------func_name----------: ", func_name)
    environ["RAV_TEST_STATUS"] = "1"
    environ["RAV_CURRENT_RUNNING_TEST"] = func_name
    html_log.h_log(message="STD Paragraph: {}   Scenario: {}".format(test_dict[func_name], strip_brackets(func_name)),
                   style=html_names.title_test)
    html_log.simple_log(html_code.start_new_table)
    environ["RAV_INTERNAL_ERROR"] = "0"


def in_case_wallpaper_was_changed():
    if environ.get("RAV_BLACK_WALLPAPPER"):
        if not int(environ.get("RAV_BLACK_WALLPAPPER")):
            test.set_black_wallpapper()


def finish_test_output_table():
    print("$$$$$$$$$$$$$$$ - in finish_test_output_table")
    print("if steps.get_step_counter() == 0 and int(environ.get(\"RAV_TEST_STATUS\")), "
          "step_counter{},rav status{}".format(steps.get_step_counter(),int(environ.get(
        "RAV_TEST_STATUS"))))
    # if steps.get_step_counter() == 0 and int(environ.get("RAV_TEST_STATUS"))==1:
    #     html_log.h_log(message="SUCCESS", style=html_names.success_class,
    #                tag_first=html_code.start_new_cell, tag_last=html_code.end_cell,
    #                new_line=False)
    if int(environ.get("RAV_TEST_STATUS"))==1:
        html_log.h_log(message="SUCCESS", style=html_names.success_class,
                   tag_first=html_code.start_new_cell, tag_last=html_code.end_cell,
                   new_line=False)
    html_log.simple_log(html_code.end_row)
    html_log.simple_log(html_code.end_table)
    html_log.h_log("")
    html_log.h_log("")
    html_log.h_log("")
    steps.step_init(0)


def in_case_of_internal_error(func_name):
    if int(environ.get("RAV_TEST_STATUS")) == 1 and steps.get_step_counter() > 0:
        environ["RAV_TEST_STATUS"] = "0"
        print("DEBUG: func_name {} steps number on tear down {}".format(func_name,
                                                                        steps.get_step_counter()))
        html_log.h_log(message="     Internal error occured, pleased contact Automation team",
                       style=html_names.internal_failure_message,
                       tag_first=html_code.start_new_cell, tag_last=html_code.end_cell, new_line=False)
        print_paths_on_test_failure()
        steps.step_init(0)


def take_screenshot_on_error(error_message):
    print("Test is Failed - taking screenshot to report")
    environ["RAV_TEST_STATUS"] = "0"
    func_name = inspect.stack()[1][3]
    print("func_name ", func_name)
    html_log.simple_log(html_code.start_new_cell)
    html_log.h_log("---->FAIL:     {}".format(error_message),style=html_names.failure_message)
    print_paths_on_test_failure()
    failures_folder_path = environ['DEVICE_FAILURE_FOLDER']
    func_name = strip_brackets(error_message)
    img_path = failures_folder_path + "/{}.png".format(os.environ.get("RAV_RUNNING_SCENARIO"))
    img_path = img_path.replace(" ", "_")
    settings.adb_functions.screenshot_by_adb(img_path)
    #img_path = "file:///" + img_path
    img_path = html_log.convert_message_to_path(img_path)
    html_log.h_log(message="     screenshot on error: ",
                   link=img_path,
                   style=html_code.failure_message,
                   tag_first=html_code.start_new_cell,
                   tag_last=html_code.end_cell,
                   new_line=False)
    try:
        utility.if_questionary_skip()
        interface.dismiss_allow_to_share_installed_applications()
        interface.skip_rating()
        test.allow_permission()
    except Exception as e:
        print(e)
    return error_message


def start_recording() -> int:
    print("in start_recording")
    # print("PID from os.environ before condition: ", environ['PID'])
    # stop_recording()
    # if environ.get('DISABLE_TEST_RECORDING') == "1":
    #     print("test recording disabled")
    #     return -1

    print("recording now")
    try:
        result = Popen("adb -s {} shell screenrecord  --time-limit 180 "
                       "--bit-rate 400000  {}".format(device_id, device_video_path).split())
        print("video_recording pid={}".format(result.pid))
        print("popen of screenrecord command ",result)
        time.sleep(2)
        os.environ["RAV_CURRENT_VIDEO_PROCESS_ID"] = str(result.pid)
        return result.pid
    except Exception as e:
        print("got an exception")
        print(e)
        return -1


def stop_recording(p_id: int = None, save_path: Path=None, save_anyway = False,save_name=None) -> None:
    print("in stop recording , p_id: {}",p_id)
    print("in stop recording , path: {} ", save_path)
    p_id = os.environ.get("RAV_CURRENT_VIDEO_PROCESS_ID")
    if save_name:
        save_path = Path(config.videos_folder) / save_name
    if p_id:
        p_id = int(p_id)
        if p_id == -1:
            return
        try:
            kill(p_id, signal.SIGTERM)
        except Exception as e:
            print(e)
    time.sleep(4)
    # Save video
    save_anyway = True
    try:
        print("environ.get(RAV_TEST_STATUS) = ",environ.get("RAV_TEST_STATUS"))
        if environ.get("RAV_TEST_STATUS") == "0" or save_anyway:
            print("saving video on server ")
            check_output("adb -s {} pull {} {}.mp4".format(device_id, device_video_path, save_path).split())
    except Exception as e:
        print(e)


test_dict = {
             '<Scenario "install start">': "1.1",
             '<Scenario "Test1">':"0.0",
             '<Scenario "allow permissions on start first installation">': "1.2",
             '<Scenario "dismiss all permissions and allow all permissions">':"1.2",
             '<Scenario "dismiss all permissions">':"1.2",
             '<Scenario "change clock size">':"1.7",
             '<Scenario "setup color widget">':"1.7",
             '<Scenario "enable clock widget">':"1.7",
             '<Scenario "drag widget">':"1.7",
             '<Scenario "drag widget to trash">':"1.7",
             "<Scenario \"disable show hints\">":"1.11",
             "<Scenario \"enable full screen\">": "1.11",
             "<Scenario \"disable show settings icon\">": "1.11",
             '<Scenario "open contact us">': "1.13",
             '<Scenario "open tutorial">': "1.16",
             '<Scenario "disable and enable start">':"1.20",
             '<Scenario "enable start">': "1.20",
             '<Scenario "disable start">': "1.20",
             '<Scenario "multiple ring clicks">': "1.23",
             '<Scenario "multiple screen off and on">': "1.23",
             '<Scenario "multiple press on back button">': "1.23",
             '<Scenario "multiple press on home button">': "1.23",
             '<Scenario "multiple press on menu icon">': "1.23",
             '<Scenario "back from first drawer">': "1.24",
             '<Scenario "back from second drawer">': "1.24",
             '<Scenario "back from settings">': "1.24",
             '<Scenario "back from security">': "1.24",
             '<Scenario "Change device language">': "1.6",
             '<Scenario "Change Start Language">': "1.6",
             '<Scenario "Change language on Start and device">':"1.6",
             '<Scenario "clicking on settings button and getting to wrong screen">': "Issue No 1"

             }
#do_at_session_start()