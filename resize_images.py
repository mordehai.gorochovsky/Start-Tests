import os
from pathlib import Path

import cv2
from PIL import Image

from settings import adb_functions


class ResizeImages:
    def __init__(self):
        self.adb = adb_functions

    def set_images_size(self):
        screen_width, screen_height = self.adb.get_physical_device_screen_size()
        print("screen_width, screen_height:", screen_width, screen_height)

        dpi_size = self.adb.get_device_dpi()
        print(type(dpi_size), dpi_size)
        # if screen_width != 1080 or screen_height != 1920:
        if dpi_size != 480:
            print("dpi size not 480 need to resize images")
            for file_name, size in self.get_image_size_and_the_new_size_for_resize().items():
                self.resize_images(file_name, *size)

            return True
        return False

    @staticmethod
    def resize_images(file_name, width, height):
        print("file name to be resize:", file_name)
        im = cv2.imread(file_name)
        print("width, height", width, height)
        thumbnail = cv2.resize(im, (width, height))
        cv2.imwrite("{}/resize_images/{}.png".
                    format(Path.home(), Path(file_name).stem), thumbnail)

    def get_image_size_and_the_new_size_for_resize(self):
        path = str(Path.home()) + "/Pictures"
        print(path)
        folders = ["{}/resize_images/".format(path)]
        images_size = {}
        for folder in folders:
            self.collect_size(folder, images_size)
        return images_size

    def collect_size(self, folder, images_size):
        for (pth, dirs, files) in os.walk(folder):
            for fileName in files:
                Image.open("{}".format(folder) + fileName)
                with Image.open(os.path.join(pth, fileName)) as image:
                    image_size = image.size
                    images_size[folder + fileName] = self.get_new_image_size(folder,
                                                                             *image_size)

    def ratio_dpi_to_new_image_size(self):
        dpi_size = self.adb.get_device_dpi()
        if dpi_size == 320:
            return 1.5
        if dpi_size == 420:
            return 1.143
        if dpi_size == 240:
            return 2
        if dpi_size == 160:
            return 3
        if dpi_size == 120:
            return 4
        if dpi_size == 640:
            return 1.33
        elif dpi_size >= 480:
            return dpi_size / 480
        elif dpi_size < 480:
            return 480 / dpi_size

    def get_new_image_size(self, folder, width, height):
        screen_width, screen_height = self.adb.get_physical_device_screen_size()
        if "large_images" in folder:
            return screen_width, screen_height
        size_action = self.ratio_dpi_to_new_image_size()
        if size_action == 1.33:
            width = round(int(width * size_action))
            height = round(int(height * size_action))
            return width, height
        # width = round(screen_width / 100 * (width * 100 / 1080))
        # height = round(screen_height / 100 * (height * 100 / 1920))
        width = round(int(width / size_action))
        height = round(int(height / size_action))
        return width, height
