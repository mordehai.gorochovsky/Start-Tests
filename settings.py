import os
from uiautomator import Device
import sys
sys.path.append('../../')
device_id = os.environ.get("device_running_id")
device_name = os.environ['device_running_name']
device_names = device_id + "_" + device_name
device_folder_name = device_names.replace(" ", "_")
device_obj = Device(device_id)
from adb_functions import AdbFunctions

adb_functions = AdbFunctions()
import uiautomator_canvas
CanvasAutomation = uiautomator_canvas.CanvasAutomation
#from uiautomator_canvas import CanvasAutomation
canvas = CanvasAutomation()
from start_functions import StartFunctions

test = StartFunctions()
