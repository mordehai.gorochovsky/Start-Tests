import os
from pathlib import Path
#from sets import
try:
    device_id = os.environ.get("device_running_id")
    device_name = os.environ['device_running_name']
    device_names = device_id + "_" + device_name
    device_folder_name = device_names.replace(" ", "_")
    device_folder_path = Path(os.environ.get("HOME_DIRECTORY")) / "{}".format(device_folder_name)
    hubs_screenshot_path = [str(device_folder_path / "multimedia_apps_page.png"),
                            str(device_folder_path / "messaging_apps_page.png"),
                            str(device_folder_path / "plus_icon_page.png")]
    videos_folder = Path(os.environ['DEVICE_FAILURE_FOLDER']) / "videos"
except Exception as e:
    print(e)
devices_with_factory_Start = ["V00776141045040"]
devices_to_disable_clock_and_date_widgets = ["ada140cf"]
home_directory_to_email_it  = "share/workspace"
locall_host_ip = "172.17.252.239"
change_to_standard_theme = ["ONGIZTTW8H6SJFWW","ada140cf"]
hubs = ["messaging_hub_icon.png", "camera_hub_icon.png",
        "unlock_hub_icon.png", "favorite.png", "search_hub_icon.png"]

hubs_hints = {"lock_hub":"Slide",
              "message_hub": "Select messaging apps",
              "multimedia_hub": "Select multimedia apps",
              "unlock_hub": "Unlock",
              "favorite_hub": "Select favorite apps",
              "contact_hub": "Select contact",
              "search_hub": "Search for results"}

message_app = ["SMS", "Message", "sms", "Messaging", "Messages", "Aﬂessages", "AAessages",
               "KAessages"]
abstract = ["Abstract","ABSTRACT","Abstract "]
video_app = ["Video", "Movies"]
gallery_app = ["Gallery", "Album"]
plus_icon = ["Plus", "Choose app shortcut"]
email_app = ["Email", "Mail"]
camera_app = ["Camera", "camera", "Open Camera"]
youtube_app = ["Youtube", "YouTu be"]
wallpaper = ["WALLPAPERS"]
categories = ["CATEGORIES","Categories","categories"]
print_test_steps = True

false_steps = ["acquire_icons_coordinates"]


adv_set_relative_layout_class = "android.widget.RelativeLayout"
adv_set_linear_layout_class = "android.widget.LinearLayout"
adv_set_chk_box_class = "android.widget.CheckBox"


class setup_wallpaper_page_elements:
    level1_frame = "android.widget.FrameLayout"
    level2_list = "android.widget.ListView"
    level3_relative = "android.widget.RelativeLayout"

class more_wallpapers:
    framw_1 = "android.widget.FrameLayout"
    listv_2 = "android.widget.ListView"
    relative_3 = "android.widget.RelativeLayout"
    linear_4 = "android.widget.LinearLayout"
    textv_5 = "android.widget.TextView"

class general_class_names:
    framw = "android.widget.FrameLayout"
    textv = "android.widget.TextView"



class Settings_dict:
    setup_wallpaper = "Setup Your Wallpaper"


#THEME NAMES:
black_theme_name = "The_New_Black"


#SCREEN TITLES:
choose_your_wallpaper = "Choose Your Wallpaper"


#Widgets
ClockWidget = "Clock Widget"
WidgetAlignmemt = "Widget Alignment"
WidgetColor= "Widget Color"
WidgetSize = "Widget Size"
# for (pth, dirs, files) in os.walk(device_folder_path):
#     for fileName in files:


lock_points = {'4200d975cf009300':(271,796),"V00776141045040":(243,613)}

abstract_description = ["ABSTRACT", "Abstract ","Abstract"]

abstract_page_img_name = "z_themes_abstract.png"
abstract_page_img_name_cropped = "z_themes_abstract_cropped.png"

camera_ids = set(["com.android.gallery3d:id/shutter_button_video","com.mediatek.camera:id/shutter_button_video","com.google.android.GoogleCamera:id/camera_app_root",
              "com.google.android.GoogleCamera:id/camera_app_root","com.mediatek.camera:id/shutter_button_video","com.android.gallery3d:id/shutter_button_video",
              "com.myos.camera:id/shutter_button_video","com.huawei.camera:id/shutter_button","com.sec.android.app.camera:id/GLSurfaceLayout"])

languages = {"Arabic": ["ar","الإعدادات"],
"Chinese": ["zh-rCN","设置"],
"Chinese_Traditional": "zh-rTW",
"Danish":"da",
"Dutch":"du",
"English":["en","Settings"],
"English_India":"en_IN",
"Finnish":"fi",
"French": ["fr","Paramètres"],
"German":["de","Einstellungen"],
"Greek":"el",
"Hebrew":["iw","הגדרות"],
"Italian":["it","Impostazioni"],
"Japanese":["ja","設定"],
"Korean":"ko",
"Norwegian":"no",
"Polish":"po",
"Portuguese": ["pt","Configurações"],
"Portuguese_Brazilian": "pt-rBR",
"Russian": ["ru","Настройки"],
"Spanish":["es","Preferencias"],
"Spanish_Mexican":["es_MX",""],
"Swedish":"se",
"Thai":"th",
"Turkish":"tr",
"Czech":"cs",
"Czech_Czech_Republic":"cs_CZ",
"Hungarian_Hungary":"hu_HU",
"Hindi_India":"hi_IN"
 }

settings_languages = {"Spanish":"Español","English":"English","French":"Français","Italian":"Italiano",
                      "Portuguese":"Português","Russsian":"Русский","Arabic":"العربية","Hebrew":"עברית",
                      "German":"Deutsch","Japanese":"日本語"}

apk_change_language = "/home/share/workspace/apk_change_language/ADB_Change_Language_v0.80_apkpure.com.apk"


