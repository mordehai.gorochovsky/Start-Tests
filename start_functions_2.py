import inspect
import os
import smtplib
import time
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from subprocess import check_output
from uiautomator import Device

import config
#from adb_functions import log_html

device_id = os.environ.get("device_running_id")
device_obj = Device(device_id)


def restart_adb_server():
    check_output("adb kill-server".split())
    time.sleep(1)
    check_output("adb start-server".split())
    time.sleep(1)


def long_swipe_on_point_from_point(pt_from=None, pt_on=None, n_swipes=None,back_to_from=False):
    """
     start on some point than move to another point and press it for some time
    """
    print("in long_swipe_on_point_from_point ")
    print("pt_from ",pt_from," pt_on: ",pt_on,)
    if n_swipes:
        number_of_swipes_on_lock = n_swipes
    else:
        number_of_swipes_on_lock = 50
    points_array = []
    if pt_from:
        points_array = [pt_from]
    arr = build_point_array(number_of_swipes_on_lock,pt_on)#get_points_arr_close_to_point(number_of_swipes_on_lock, pt_on)
    print("arr to append " , arr)
    points_array.extend(arr)
    if back_to_from:
        points_array.append(pt_from)
        #device_obj.swipe(points_array[-1][0], points_array[-1][1], pt_from[0], pt_from[1])
    print("points array at end:", points_array)
    try:
        device_obj.swipePoints(points_array, steps=20)

    except Exception as e:
        print(e)
        restart_adb_server()
        device_obj.swipePoints(points_array, steps=20)


def get_points_arr_close_to_point(number_of_swipes_on_lock, pt_on):
    flag = 0
    arr = []
    point_on_1 = pt_on
    point_on_2 = (pt_on[0] + 1, pt_on[1] + 1)
    for i in range(number_of_swipes_on_lock):
        if flag == 0:
            arr.append(point_on_1)
            flag = 1
        else:
            arr.append(point_on_2)
            flag = 0
    return arr


def long_swipe_on_point_to_point(pt_to=None, pt_on=None, n_swipes=20):
    # ---long press on some point than move to another point
    points_array = []
    if n_swipes:
        number_of_swipes_on_lock = n_swipes
    else:
        number_of_swipes_on_lock = 50
    # arr = []
    # point_on_1 = pt_on
    # point_on_2 = (pt_on[0] + 1, pt_on[1] + 1)
    # #print("p1 = {} , p2 = {}".format(point_on_1,point_on_2))
    # #print("pt_to ",pt_to)
    # flag = 0
    # print("n swipes = ",number_of_swipes_on_lock)
    # for i in range(number_of_swipes_on_lock):
    #     if flag == 0:
    #         arr.append(point_on_1)
    #         flag = 1
    #     else:
    #         arr.append(point_on_2)
    #         flag = 0
    arr = get_points_arr_close_to_point(number_of_swipes_on_lock, pt_on)
    if pt_to:
        points_array.extend(arr)
    print("points array at end:", points_array)
    try:
        device_obj.swipePoints(points_array, steps=20)
    except Exception as e:
        print(e)
        restart_adb_server()
        device_obj.swipePoints(points_array, steps=20)


def build_point_array(num_of_points, point):
    print("in build_point_array")
    print("point ", point, " num_of_points ",num_of_points)
    points_array = []
    flag = 0
    for i in range(num_of_points):
        if flag == 0:
            points_array.append(point)
            flag = 1
        else:
            points_array.append((point[0]+1, point[1]+1))
            flag = 0
    return points_array


def three_steps_swipe(pt1, hub_point, arr_points, hub_name, screen_height):
    points_array = [pt1, hub_point]
    points_array = check_hub_place_and_build_stay_point(pt1, hub_point, hub_name, arr_points,
                                                        points_array, screen_height)
    print("points array end {}".format(points_array))
    print("three steps swipe starts for {} hub time: {}".format(hub_name, time.time()))
    device_obj.swipePoints(points_array, steps=55)
    print("three steps swipe ends time {}...".format(time.time()))


def check_hub_place_and_build_stay_point(pt1, hub_point, hub_name, arr_points, points_array,
                                         screen_height):
    if type(arr_points) == tuple:
        tmp = [arr_points]
        arr_points = tmp
    print("DEBUG hub name:", hub_name)
    if hub_name == "multimedia":
        middle_center = (pt1[0] + int(pt1[0] / 7), int(screen_height * 0.65))
        middle_array = build_point_array(10, middle_center)
        print("middle_array", middle_array)
        for pt in arr_points:
            points_array.append((pt[0], pt[1]))
            points_array.extend(middle_array)
    elif hub_name == "messaging" or hub_name == "plus_icon":
        for pt in arr_points:
            pt = (pt[0], pt[1])
            points_array.append(pt)
            if hub_point[0] < pt[0]:
                x = int(((hub_point[0]*1.1 + pt[0]*0.9) / 2))
                y = int(((hub_point[1]*1.1 + pt[1]*0.9) / 2))
            else:
                x = int(((hub_point[0]*1.1 + pt[0]*0.9) / 2))
                y = int(((hub_point[1]*1.1 + pt[1]*0.9) / 2))
            stay_point = (x, y)
            middle_array = build_point_array(10, stay_point)
            points_array.extend(middle_array)
    points_array.append(pt1)
    return points_array


def send_mail(to, text):
    print("send mail")
    msg = MIMEMultipart()
    msg['From'] = "autoravtech@gmail.com"
    msg['To'] = to
    msg['Subject'] = text
    test_directory_name = str(os.environ["RAVTECH_NOW"]).replace(" ", "_").replace(":",
                                                                                   "_")
    tmp = os.environ['test_failures_path'] + "/" + "test_log_" + test_directory_name + ".html"
    path = [tmp]
    msg.attach(MIMEText(text))
    for i in path:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(i, 'rb').read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(i))
        msg.attach(part)

    gmail_user = "autoravtech@gmail.com"
    gmail_pwd = "ravtech1234"
    mail_server = smtplib.SMTP("smtp.gmail.com", '587')

    mail_server.ehlo()

    mail_server.starttls()

    mail_server.ehlo()

    mail_server.login(gmail_user, gmail_pwd)

    mail_server.sendmail(gmail_user, to.split(","), msg.as_string())

    # Should be mailServer.quit(), but that crashes...
    mail_server.close()


def decrease_widget_given_by_index(indx,num_of_clicks):
    r_id = "com.celltick.lockscreen:id/widget_sizee_id"
    r_id2 = "com.celltick.lockscreen:id/pref_btn_left"
    for ind, item in enumerate(device_obj(resourceId=r_id)):
        if ind == indx:
            while num_of_clicks > 0:
                try:
                    item.sibling(resourceId=r_id2).click()
                except Exception as e:
                    print("error", e)
                num_of_clicks -= 1


def increase_widget_given_by_index(indx,num_of_clicks):
    r_id = "com.celltick.lockscreen:id/widget_sizee_id"
    r_id2 = "com.celltick.lockscreen:id/pref_btn_right"
    for ind, item in enumerate(device_obj(resourceId=r_id)):
        if ind == indx:
            while num_of_clicks > 0:
                try:
                    item.sibling(resourceId=r_id2).click()
                except Exception as e:
                    print("error", e)
                num_of_clicks -= 1
    device_obj(resourceId="com.celltick.lockscreen:id/button_done_id").click()


def increase_decease_clock_widget(decrease=True, widget_index=0, num_of_clicks=50):
    print("in increase_decease_widget")
    layer_list_v_1 = "android.widget.ListView"
    layer_linear_2 = "android.widget.LinearLayout"  # index=0
    layer_linear_3 = "android.widget.LinearLayout"
    # done_btn = "com.celltick.lockscreen:id/button_done_id"
    if decrease:
        button_4_r_id = "com.celltick.lockscreen:id/pref_btn_left"
    else:
        button_4_r_id = "com.celltick.lockscreen:id/pref_btn_right"
    flag = 0
    for i in range(num_of_clicks):
        if flag == 3:
            return False
        # restart_adb_server()
        time.sleep(0.1)
        try:
            device_obj(className=layer_list_v_1).child(className=layer_linear_2, index=widget_index).child(
                className=layer_linear_3).child(resourceId=button_4_r_id).click()
        except Exception as e:
            print(e)
            return False
    device_obj(resourceId="com.celltick.lockscreen:id/button_done_id").click()
    return True


# def press_back(logging=False,behave=False):
#     if logging:
#         log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3], behave)
#     device_obj.press.back()


def press_home():
    device_obj.press.home()


def get_menu_true_menu_point(w, h):
    y = int(h * 0.075)
    x = int(w * 0.926)
    print("menu guessed  point x = {} y = {}",x,y)
    return x, y


def get_upper_starter_point(w, h):
    y = int(h * 0.075)
    x = int(w * 0.08)
    return x, y


def get_second_starter(w,h):
    y = int(h * 0.185)
    x = int(w * 0.08)
    return x, y


def get_clock_widget_coordinates(w, h):
    x = int(w/2)
    y = int(h*0.2)
    return x, y


def widget_alignment_click_on_default():
    list_view_class = "android.widget.ListView"
    list_item_class = "android.widget.CheckedTextView"
    time.sleep(2)
    a = device_obj(className=list_view_class).child(className=list_item_class, index=0).text
    if "Default" in a:
        try:
            device_obj(className=list_view_class).child(className=list_item_class, index=0).click()
            return True
        except Exception as e:
            print(e)
            return False
    else:
        print('could not find "Wallpaper Default" ')
        return False


def verify_element_exists_by_resource_id(r_id,timeout):
    print("in verify_element_exists_by_resource_id ")
    end_time = time.time() + float(timeout)
    try:
        while time.time() < end_time:
            if len(device_obj(resourceId=r_id)) > 0:
                return True
            time.sleep(1)
        return False
    except Exception as e:
        print(e)
        return False


def open_start_without_verification():
    try:
        res = check_output("adb -s {} shell am start com.celltick.lockscreen".format(device_id).split())
        if "error" in str(res).lower():
            res = check_output("adb -s {} shell am start com.celltick.lockscreen".format(device_id).split())
    except Exception as e:
        print(e)


def verify_element_exists_by_description(desc, timeout):
    end_time = time.time() + timeout
    while time.time() < end_time:
        if len(device_obj(description=desc))>0:
            return True
        time.sleep(0.5)
    return False


def click_by_description(desc):
    print("in click_by_description")
    res = verify_element_exists_by_description(desc, 15)
    if not res:
        return
    device_obj(description=desc).click()


def verify_security_screen_rooted():
    r_id2 = "android:id/action_bar"
    cls = "android.widget.TextView"
    try:
        txt = device_obj(resourceId=r_id2).child(className=cls).text
        print("text in verify_security_screen_rooted ", txt)
        if "unlock" in txt.lower():
            return True
        return False
    except Exception as e:
        print(e)
        return False


def verify_security_screen_not_rooted():
    r_id = "android:id/title"
    txt = device_obj(resourceId=r_id).text
    print("text in verify_security_screen_not_rooted ", txt)
    if "Security" in txt or "security" in txt:
        return True
    return False


def disable_widget(widget):
    print("widget to disable ", widget)
    element = device_obj(text=widget).right(resourceId="android:id/checkbox")
    date = element.info
    if date["checked"]:
        element.click()


def enable_widget(widget):
    print("widget to enable ", widget)
    element = device_obj(text=widget).right(resourceId="android:id/checkbox")
    item = element.info
    if not item["checked"]:
        element.click()
#
#
# def click_on_widget(widget):
#     print("widget to click on ", widget)
#     element = device_obj(text=widget).right(resourceId="android:id/checkbox")
#     element.click()


def click_on_categories():
    print("in click_on_categories")
    for category in config.categories:
        print("category ", category)
        if try_categories(category):
            return


def try_categories(cat):
    print("in try_categories")
    try:
        print("try category: ", cat)
        device_obj(text=cat).click()
        return True
    except Exception as e:
        print(e)
        return False


def skip_rating():
    res = verify_element_exists_by_resource_id("com.celltick.lockscreen:id/rating_bar", 2)
    if res:
        device_obj(resourceId="com.celltick.lockscreen:id/rating_bar").click()
        time.sleep(1)
        device_obj(resourceId="com.celltick.lockscreen:id/send_btn").click()
        time.sleep(1)

def if_questionary_skip():
    print("in if_questionary_skip")
    f = device_obj(resourceId="com.celltick.lockscreen:id/questionnaire_view_pager")
    if len(f) > 0:
        try:
            device_obj(text="Male").click()
        except Exception as e:
            print(e)
            device_obj(text="male").click()
        try:
            time.sleep(1)
            device_obj(text="Under 24").click()
        except Exception as e:
            print(e)
            device_obj(text="under 24").click()
        try:
            time.sleep(1)
            device_obj(text="Travel").click()
        except Exception as e:
            print(e)
            device_obj(text="travel").click()
            time.sleep(1)
        device_obj.press.home()


def verify_clock_displayed_by_extracted_text(result):
    print("in verify_clock_displayed_by_extracted_text")
    print("text  in verify_clock_displayed ",result)
    possible_results = [":","AM","PM","AAA","PAA","PI""am","pm","mm"]
    for res in possible_results:
        if res in result:
            return True
    return False


# def skip_questionary():
#     res = verify_element_exists_by_resource_id("com.celltick.lockscreen:id/questionnaire_view_pager",2)
#     if res:
#         device_obj(text="Male").click()
#         time.sleep(1)
#         device_obj(text="Under 24").click()
#         time.sleep(1)
#         device_obj(text="Travel").click()
#         time.sleep(1)
#         device_obj(resourceId="com.celltick.lockscreen:id/confirmation_button").click()


class WidgetColorUtilities:
    def __init__(self):
        self.layer_1 = "android.widget.FrameLayout"
        self.layer_2 = "android.widget.RelativeLayout"
        self.layer_3 = "android.widget.ImageView"
        self.ok_button = "android.widget.Button"
        self.circle_pointer_id = "com.celltick.lockscreen:id/ambilwarna_target"
        self.rect_id = "com.celltick.lockscreen:id/ambilwarna_viewSatBri"

    @staticmethod
    def get_middle_point(bounds):
        x1 = bounds["left"]
        x2 = bounds["right"]
        y1 = bounds["top"]
        y2 = bounds["bottom"]
        print(x1, " ", x2, " ", y1, " ", y2)
        x_3 = int((x1 + x2) / 2)
        y_3 = int((y1 + y2) / 2)
        return x_3, y_3

    @staticmethod
    def get_upper_left_point(bounds):
        print(bounds)
        x1 = bounds["left"]
        x2 = bounds["right"]
        y1 = bounds["top"]
        y2 = bounds["bottom"]
        x_4 = x1 + int((x2 - x1) / 50)
        y_4 = y1 + int((y2 - y1) / 50)
        return x_4, y_4

    @staticmethod
    def get_bottom_left_point(bounds):
        print(bounds)
        x1 = bounds["left"]
        x2 = bounds["right"]
        y1 = bounds["top"]
        y2 = bounds["bottom"]
        x_4 = x1 + int((x2 - x1) / 50)
        y_4 = y2 - int((y2 - y1) / 50)
        return x_4, y_4

    @staticmethod
    def get_width(bounds):
        x1 = bounds["left"]
        x2 = bounds["right"]
        # y1 = bounds["top"]
        # y2 = bounds["bottom"]
        result = x2 - x1
        print("x2: ", x2, " x1: ", x1, " x2-x1: ", result)
        return result

    def get_bounds_of_color_widget_objects(self):
        bounds = []
        objects = device_obj(className=self.layer_1).child(className=self.layer_2)

        print("In get_bounds_of_color_widget_objects, objects=", objects)
        num_of_obj = len(objects)
        for i in range(num_of_obj):
            objects = device_obj(className=self.layer_1).child(className=self.layer_2).child(
                index=i)
            print("OO objects", objects)
            bounds.append(objects[0].info["bounds"])
        return bounds

    def bounds_bubble_sort(self, bounds):
        print("in bubble sort , bounds=", bounds, "len = ", len(bounds))
        for i in range(len(bounds) - 1):
            if self.get_width(bounds[i]) > self.get_width(bounds[i + 1]):
                tmp = bounds[i]
                bounds[i] = bounds[i + 1]
                bounds[i + 1] = tmp
        print("returning bounds:", bounds)
        return bounds

    def get_circle_pointer_coordinates(self, bounds_array=None):
        if not bounds_array:
            bounds_array = self.get_bounds_of_color_widget_objects()
            bounds_array = self.bounds_bubble_sort(bounds_array)
        print("In get circle_pointer_coordinates, bounds = ", bounds_array)
        if bounds_array:
            x1, y1 = self.get_middle_point(bounds_array[1])
            colors_rect_bounds = bounds_array[len(bounds_array)-1]
        else:
            objects3 = device_obj(className=self.layer_1).child(className=self.layer_2). \
                child(resourceId=self.circle_pointer_id).info["bounds"]
            x1, y1 = self.get_middle_point(objects3)
            colors_rect_bounds = device_obj(className=self.layer_1).child(className=self.layer_2). \
                child(resourceId=self.rect_id).info["bounds"]

        x2_l = colors_rect_bounds["left"]
        x2_r = colors_rect_bounds["right"]
        y2_t = colors_rect_bounds["top"]
        y2_b = colors_rect_bounds["bottom"]

        if x1 <= x2_l:
            x1 = x2_l + 1
        if x1 >= x2_r:
            x1 = x2_r - 1
        if y1 <= y2_t:
            y1 = y2_t + 1
        if y1 >= y2_b:
            y1 = y2_b - 1

        return x1, y1

    def swipe_to_white_region(self):
        bounds_array = self.get_bounds_of_color_widget_objects()
        bounds_array = self.bounds_bubble_sort(bounds_array)
        x_1, y_1 = self.get_circle_pointer_coordinates(bounds_array)
        index = len(bounds_array)-1
        x_2, y_2 = self.get_upper_left_point(bounds_array[index])
        device_obj.swipe(x_1, y_1, x_2, y_2)

    def swipe_to_black_region(self):
        print(1)

        print(1.5)

        bounds_array = self.get_bounds_of_color_widget_objects()
        if not bounds_array:
            print("bounds array is empty restarting adb_server")
            restart_adb_server()
            bounds_array = self.get_bounds_of_color_widget_objects()
        print(2)
        print("bounds array before bubble sort ", bounds_array)
        bounds_array = self.bounds_bubble_sort(bounds_array)
        print(3)
        print(bounds_array)
        print(3.5)
        x_1, y_1 = self.get_circle_pointer_coordinates(bounds_array)
        print(4)
        index = len(bounds_array) - 1
        x_2, y_2 = self.get_bottom_left_point(bounds_array[index])
        print(5)
        device_obj.swipe(x_1, y_1, x_2, y_2)
        print(6)

    def click_ok_button(self):
        device_obj(className=self.ok_button, index=0).click()


class PhoneEvents:
    @staticmethod
    def parse_sim_devices(phones):
        phones_arr = phones.split(",")
        phone_book = []
        for phone in phones_arr:
            details = phone.split(":")
            if len(details) < 2:
                return {"id": -1}
            phone_book.append(dict(id=details[0], tel=details[1]))
        return phone_book

    def get_device_phone_number(self, phone_book=None):
        number = self.get_phone_number_from_environment_variable()
        if not number:
            self.call_device_from_device(os.environ.get('RAV_SERVER_PHONE_NUMBER'),device_id)
            time.sleep(10)
            self.dismiss_call_on_server()
            number = self.get_incoming_number_from_server()
            self.set_phone_number(number)
        return number
        # if not phone_book:
        #     devices_phone_numbers = os.environ.get("RAV_PHONE_NUMBERS")
        #     phone_book = self.parse_sim_devices(devices_phone_numbers)
        # for entry in phone_book:
        #     if entry["id"] == device_id:
        #         return entry["tel"]
        # print("Device phone number was not founded , verify the phone was added to Jenkins")
        # return -1

    @staticmethod
    def get_phone_number_from_environment_variable():
        env_var = "RAV" + "_" + str(device_id) + "PHONE_NUMBER"
        num = os.environ.get(env_var)
        return num

    @staticmethod
    def set_phone_number(number):
        env_var = "RAV" + "_" + str(device_id) + "PHONE_NUMBER"
        num = os.environ[env_var] = number


    @staticmethod
    def dismiss_call_on_server():
        try:
            cmd = "adb -s {} shell input keyevent KEYCODE_ENDCALL".format(os.environ.get('RAV_SERVER_PHONE_ID'))
            check_output(cmd.split())
        except Exception as e:
            print(e)
            try:
                cmd2 = "adb -s {} shell input keyevent 6".format(os.environ.get('RAV_SERVER_PHONE_ID'))
                check_output(cmd2.split())
            except Exception as e:
                print(e)


    @staticmethod
    def get_calling_device_id(phone_book, current_device_phone_number):
        for phone in phone_book:
            if phone["tel"] != current_device_phone_number:
                return phone["id"]
            print("no device to call from , possibly duplicate phone numbers")
            return -1

    @staticmethod
    def call_device_from_device(device_phone_number, calling_device_id):
        cmd = "adb -s {} shell am start -a android.intent.action.CALL -d tel:{}".format(
            calling_device_id, device_phone_number).split()
        check_output(cmd)

    def call_to_device(self):
        # devices_phone_numbers = os.environ.get("RAV_PHONE_NUMBERS")
        # phone_book = self.parse_sim_devices(devices_phone_numbers)
        # if len(phone_book) < 2:
        #     print("Less than two phones with known phone numbers, can't perform test")
        #     return -1
        device_phone_number = self.get_device_phone_number()
        if not device_phone_number:
            return -1
        else:
            # calling_device_id = self.get_calling_device_id(phone_book, device_phone_number)
            # if calling_device_id == -1:
            #     return -1
            self.call_device_from_device(device_phone_number, os.environ.get('RAV_SERVER_PHONE_ID'))
        return True

    @staticmethod
    def answer_call():
        time.sleep(10)
        cmd = "adb  -s {}  shell input key event 5".format(device_id).split()
        check_output(cmd)
        return True

    @staticmethod
    def get_incoming_number_from_server():
        print("in get_incoming_number_from_server")
        cmd = "adb -s {} shell dumpsys | grep incoming_number".format(os.environ.get('RAV_SERVER_PHONE_ID'))
        a = check_output(cmd.split())
        b = str(a).split("=")[1]
        c = b.split(",")[0]
        print(c)
        return c

