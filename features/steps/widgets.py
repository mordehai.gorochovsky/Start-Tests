from pathlib import Path
#gg
from behave import *
import sys
sys.path.append('../')
import start_interface as interface
import start_tests_header as s_header
use_step_matcher("re")
use_step_matcher("cfparse")

@step('clicking on widget "{widget}"')
def step_impl(context,widget):
    """
    :type context: behave.runner.Context
    """
    assert interface.test.select_item_in_Setup_Your_Widget(widget,logging=True), s_header.take_screenshot_on_error('clicking on {} failed'.format(widget))


@step('disable widget - "{widget}"')
def step_impl(context,widget):
    interface.disable_widget(widget)


@step('enable widget - "{widget}"')
def step_impl(context,widget):
    interface.enable_widget(widget)


@step('disable clock widget if enabled')
def step_impl(context):
    interface.disable_clock_widget_if_enabled()


@step('enable clock widget if disabled')
def step_impl(context):
    interface.enable_clock_widget_if_disabled()

@then('verify that the clock widget displayed')
def step_impl(context):
    assert interface.verify_clock_displayed(logging=True), s_header.take_screenshot_on_error("failed to enable clock widget")


# @step('click on widget "{widget}"')
# def step_impl(context,widget):
#     interface.clik_on_widget(widget)


@then('set widgets color to black and verify change')
def step_impl(context):
    assert interface.set_widget_color_to_black_and_verify(), s_header.take_screenshot_on_error("widget color was not changed")


@then('verify in Widget Size screen')
def step_impl(context):
    assert interface.verify_in_Widget_Size_screen(), s_header.take_screenshot_on_error("not in Widget Size screen")


@step('minimize clock widget by "{percent}" percents and verify')
def step_impl(context,percent):
    assert interface.minimize_clock_widget_and_verify(num_of_clicks=int(percent)), s_header.take_screenshot_on_error("clock wasn't minimized")


@step('maximize date widget')
def step_impl(context):
    interface.maximize_date_widget()


@then('drag widget to trash and verify')
def step_impl(context):
    assert interface.drag_widget_to_trash_and_verify(num_of_clicks=100), s_header.take_screenshot_on_error("failed to drag widget")


@then('drag widget and verify')
def step_impl(context):
    assert interface.drag_widget_and_verify(num_of_clicks=100), s_header.take_screenshot_on_error("failed to drag widget")
