from pathlib import Path

from behave import *
import sys
sys.path.append('../')
import start_interface as interface
import start_tests_header as s_header
use_step_matcher("re")
use_step_matcher("cfparse")


@step("clicking on security")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    print("in clicking on security")
    assert interface.click_on_security(), s_header.take_screenshot_on_error("not in Security screen")
    #interface.click_on_security(logging=True,behave=True),s_header.take_screenshot_on_error("not in Security screen")

#
# @then("back from settings")
# def step_impl(context):
#     """
#     :type context: behave.runner.Context
#     """
#     print("in back from settings")
#     assert interface.back_from_settings_screen(behave=True), s_header.take_screenshot_on_error("not in Srart home page")

@then("back from security")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    print("in back from settings")
    assert interface.back_from_security_screen(behave=True), s_header.take_screenshot_on_error("not in Srart home page")

@when("open first drawer")
def step_impl(contetxt):
    """
    :type context: behave.runner.Context
    """
    print("in open drawer")
    assert interface.open_upper_drawer(behave=True), s_header.take_screenshot_on_error("drawer was not opened")

@when("open second drawer")
def step_impl(contetxt):
    """
    :type context: behave.runner.Context
    """
    print("in open drawer")
    assert interface.open_second_drawer(behave=True), s_header.take_screenshot_on_error("drawer was not opened")


# @then("back from drawer")
# def step_impl(contetxt):
#     """
#     :type context: behave.runner.Context
#     """
#     print("in back from drawer")
#     img = s_header.test_adb.screenshot_by_adb(Path(interface.test.device_folder) / "multiple_press_back_drawer_1.png")
#     assert interface.back_from_drawer(img,behave=True), s_header.take_screenshot_on_error("not in Srart home page")
#
#



