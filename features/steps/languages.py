from behave import *
import sys
sys.path.append('../')
import  start_interface as interface
import start_tests_header as s_header

use_step_matcher("re")
use_step_matcher("cfparse")

@given("set device language to English")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    print("llllooooooooooooooooooooooool")
    interface.change_to_english()


@step('verify "{language}" language')
def step_impl(context,language):
    """
    :type context: behave.runner.Context
    """
    res = interface.verify_language(language)
    interface.press_back_n_times(2)
    assert res, s_header.take_screenshot_on_error("not expected language")

#And set device language to "Hebrew"

@step('set device language to "{language}"')
def step_impl(context,language):
    """
    :type context: behave.runner.Context
    """
    assert interface.change_language_to(language), s_header.take_screenshot_on_error("not expected language")
#
# And go to languages screen
#     Then verify in languages screen

@step('go to languages screen')
def step_impl(context):
    interface.go_to_languages()


@then('verify in languages screen')
def step_impl(context):
    assert interface.verify_in_languages_screen(), s_header.take_screenshot_on_error("not in Languages page")

#change_start_language_to

@step('change Start language to "{language}"')
def step_impl(context,language):
    interface.change_start_language_to(language)


@step('click on Settings when Start language is "{language}"')
def step_impl(context,language):
    interface.click_on_settings_when_start_language_is(language)
