from pathlib import Path

from behave import *
import sys
sys.path.append('../')
import start_interface as interface
import start_tests_header as s_header
use_step_matcher("re")
use_step_matcher("cfparse")


@then('verify disable start appears')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    print("disable start appears")
    assert interface.verify_disable_start_opened(), s_header.take_screenshot_on_error("not in disable start screen")


@step('click on disable start choice button')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    interface.click_on_disable_start_choice_button()


@then('click on disable start choice button')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    interface.click_on_disable_start_choice_button()


@Then('verify that disable periods appear')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    assert interface.verify_disable_start_periods_apppear(), s_header.take_screenshot_on_error("diasble periods did not appear")


@step('clicking on ok button in disable start periods screen')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    interface.click_on_ok_buttton_in_disable_start_for_period_screen()#, s_header.take_screenshot_on_error("not in device home screen")



