from behave import *
import sys
sys.path.append('../')
import start_interface as interface
import start_tests_header as s_header
use_step_matcher("re")
use_step_matcher("cfparse")


@when('clicking on ring "{num}" times')
def step_impl(context,num):
     """
     :type context: behave.runner.Context
     """
     print("in clicking on ring step")
     interface.multiple_clicks_on_ring(num,verification=False,behave=True)


@when('turn screen on and off "{num}" times')
def step_impl(context,num):
    """
    :type context: behave.runner.Context
    """
    print("in turn screen on and off")
    interface.multiple_screen_on_and_off(num,verification=False,behave=True)


@when ('clicking on menu icon "{num}" times')
def step_impl(context,num):
    """
    :type context: behave.runner.Context
    """
    print("in clicking on menu icon  step")
    interface.multiple_click_on_settings_icon(num,verification=False)

@when ('clicking on back button "{num}" times')
def step_impl(context,num):
    """
    :type context: behave.runner.Context
    """
    print("in clicking on back button step")
    interface.multiple_press_back(num, pref="in_stress_2",verification=False,behave=True)

@when ('clicking on home button "{num}" times')
def step_impl(context,num):
    """
    :type context: behave.runner.Context
    """
    print("clicking on home button step")
    interface.multiple_press_home(num,verification=False,behave=True)

