# Created by itzik at 3/13/18
Feature: stress tests

  Scenario: multiple ring clicks
    Given open start
    Then stop recording and save by name "before_multiple_ring_clicks"
    And wait for "5" seconds
    Then start video recording
    When clicking on ring "20" times
    And wait for "1" seconds
    Then verify Start home page

  Scenario: multiple screen off and on
    Given open start
    Then stop recording and save by name "before_multiple_screen_of_and_on"
    And wait for "5" seconds
    Then start video recording
    When turn screen on and off "10" times
    And wait for "1" seconds
    Then verify Start home page

  Scenario: multiple press on back button
    Given open start
    Then stop recording and save by name "before_multiple_press_on_back_button"
    And wait for "5" seconds
    Then start video recording
    When clicking on back button "20" times
    Then verify Start home page

  Scenario: multiple press on home button
    Given open start
    Then stop recording and save by name "before_multiple_press_on_home_button"
    And wait for "5" seconds
    Then start video recording
    When clicking on home button "20" times
    And wait for "1" seconds
    Then verify Start home page

  Scenario: multiple press on menu icon
    Given open start
    Then stop recording and save by name "before_multiple_press_on_menu_icon"
    And wait for "5" seconds
    Then start video recording
    When clicking on menu icon "20" times
    And wait for "1" seconds
    Then verify Start home page







