import os
from time import sleep

import sys
import start_tests_header as s_header
from start_functions import  catch_any_error_and_return_false

#import start_interface as interface
#dgh
'''
before_step(context, step), after_step(context, step)
These run before and after every step.
before_scenario(context, scenario), after_scenario(context, scenario)
These run before and after each scenario is run.
before_feature(context, feature), after_feature(context, feature)
These run before and after each feature file is exercised.
before_tag(context, tag), after_tag(context, tag)
These run before and after a section tagged with the given name. They are invoked for each tag encountered in the order they’re found in the feature file. See controlling things with tags.
before_all(context), after_all(context)
These run before and after the whole shooting match.
'''
android_log_file = p_id = func_name = None
print(" f ")

#@s_header.interface.catch_any_error_and_return_false
def debug_Rav():
    s_header.enable_clock_and_date_widgets_if_needed()
    # after_scenario_change_start_language()
    # sys.exit()


def before_all(context):
    print("@@@@@@@@@in before_all ,,,")
    #debug_Rav()
    #TODO disable installing start when "do_at_session_start()" enabled
    #s_header.do_at_session_start()
    s_header.do_at_session_start_without_coordinates_acquiring()
    #TODO if "do_at_session_start()" uncomented no need to run "save_home_screen_xml"
    s_header.save_home_screen_xml()
    s_header.interface.stf_2.open_start_without_verification()


def before_scenario(context,scenario):
    print("@@@@@@@@@in before_scenario ,,, ", scenario)
    global func_name, android_log_file, p_id
    func_name = str(scenario)#s_header.strip_brackets(str(scenario))
    os.environ["RAV_RUNNING_SCENARIO"] = s_header.strip_brackets(func_name)
    print("func_name 888888  ", func_name)
    android_log_file, p_id = s_header.execute_before_scenario(func_name)
    os.environ["RAV_CURRENT_VIDEO_PROCESS_ID"] = str(p_id)
    if "permissions" not in str(scenario.name).lower():
        provoke_popups_and_close()
    if "language" in str(scenario.name).lower():
        s_header.interface.change_to_english(logging=False)
        sleep(2)
    if "drag widget" in str(scenario.name).lower():
        s_header.interface.widgets_alignment_center()
        s_header.interface.reset_widget_size()


def before_feature(context, feature):
    if feature.name == 'Widgets':
        s_header.interface.widgets_alignment_center()
        s_header.interface.reset_widget_size()
    if feature.name == "Languages":
        if not s_header.test_adb.verify_change_language_installed():
            s_header.test_adb.install_change_language()
    if feature.name == "back button":
        s_header.interface.disable_full_screen()


def after_all(context):
    print("@@@@@@@@@in after_all ,,,")
    s_header.reverse_all_changes_on_device()


'''func_name = request.function.__name__
    #test.adb.install_start_apk(log=False)
    android_log_file, p_id = execute_before_scenario(func_name)
    yield p_id
    execute_after_scenario(android_log_file, func_name, p_id)'''


def after_scenario_drag_widget(scenario):
    if "drag widget" in scenario.name:
        print("pemissions in scenario name: ", scenario.name)
        s_header.enable_clock_and_date_widgets_if_needed()



def after_scenario(context,scenario):
    print("@@@@@@@@@@in after_scenario ,,,: ", scenario )
    print("scenario name 1 ", scenario.name)
    print("scenario contains" ,dir(scenario))
    s_header.execute_after_scenario(android_log_file, func_name, p_id)
    after_scenario_change_laguage(scenario)
    after_scenario_change_start_language(scenario)
    after_scenario_settings_button_issue(scenario)
    after_scenario_allow_permissions(scenario)
    after_scenario_setup_color_widget(scenario)
    after_scenario_change_clock_size(scenario)
    after_scenario_permissions(scenario)
    after_scenario_drag_widget(scenario)
    is_tutorial = os.environ.get("RAV_TUTORIAL_AFTER_FIRST_INSTALLATION")
    if is_tutorial is not None and is_tutorial == "yes":
        ## Deprecated by Mordechai 21.03.18
        #s_header.interface.open_tutorial_and_learn_it()
        pass


def after_step(context, step):
    if "set screen state to" in str(step.name):
        print("in step___ : ",str(step))
        # dismiss = s_header.interface.dismiss_allow_to_share_installed_applications()
        # interface = s_header.interface.skip_questionary_if_needed()
        # if interface and not dismiss:
        #     s_header.interface.dismiss_allow_to_share_installed_applications()
#


def provoke_popups_and_close():
    print("in provoke_popups_and_close")
    provoke_something()
    is_share_apps_list_dissmised = str(os.environ.get("RAV_SHARE_APPS_LIST_DISMISSED")).lower()
    print("is_share_apps_list_dissmised ",is_share_apps_list_dissmised)
    if is_share_apps_list_dissmised == "false":
        print("needs to dismiss allow share apps ")
        dismissed = s_header.interface.dismiss_allow_to_share_installed_applications()
    else:
        dismissed = True
    sleep(2)
    is_questionary_answered = str(os.environ.get("RAV_QUESTIONARY_ANSWERED")).lower()
    print("is_questionary_answered ",is_questionary_answered)
    if is_questionary_answered == "false":
        print("needs to answer questionary")
        questionary = s_header.interface.skip_questionary_if_needed(logging=False)
    else:
        questionary = True
    if not dismissed  and questionary:
        provoke_something()
        s_header.interface.dismiss_allow_to_share_installed_applications()


def provoke_something():
    s_header.interface.set_screen_state_off(logging=False)
    sleep(0.5)
    s_header.interface.set_screen_state_on(logging=False)
    sleep(3)


@catch_any_error_and_return_false
def after_scenario_permissions(scenario):
    if "permissions" in scenario.name:
        print("pemissions in scenario name: ", scenario.name)
        if True:  # s_header.device_id in s_header.config.change_to_standard_theme:
            # s_header.change_to_standart_theme()
            s_header.interface.open_camera_by_swiping_to_multimedia_hub(logging=False)
            s_header.interface.allow_all_permisions()
            s_header.interface.verify_no_permission_request_displayed_for_contacts_and_physical_location(logging=False)
            s_header.interface.verify_no_permission_request_for_file_access(logging=False)


@catch_any_error_and_return_false
def after_scenario_change_clock_size(scenario):
    if "change clock size" in scenario.name:
        s_header.interface.reset_widget_size()


@catch_any_error_and_return_false
def after_scenario_setup_color_widget(scenario):
    if "setup color widget" in scenario.name:
        s_header.interface.set_widget_color_to_default()
        # s_header.interface.multiple_press_back(3,logging=False)
        # s_header.interface.swipe_and_click_on_settings(logging=False)
        # s_header.interface.click_on_Setup_Your_Widgets(logging=False)
        # s_header.interface.click_on_Widget_Color(logging=False)
        # s_header.interface.set_widget_color_to_white(logging=False)


@catch_any_error_and_return_false
def after_scenario_allow_permissions(scenario):
    if "allow permissions on start first installation" in scenario.name:
        s_header.interface.skip_keep_in_touch_screen_if_needed(logging=False)


@catch_any_error_and_return_false
def after_scenario_settings_button_issue(scenario):
    if "clicking on settings button and geting to wrong sc" in str(scenario.name):
        print("presssin back in after scenario")
        s_header.interface.press_back(logging=False)
        s_header.interface.press_back(logging=False)


@catch_any_error_and_return_false
def after_scenario_change_start_language(scenario):
    if "Change Start Language" in scenario.name or "Change language on Start and device" in scenario.name:
        s_header.test.back(4)
        s_header.test.device_obj().swipe.left()
        sleep(2)
        s_header.interface.press_on_settings_by_index()
        sleep(1)
        s_header.interface.click_on_languages_by_index()
        sleep(1)
        s_header.interface.change_start_language_to_english(logging=False)
        sleep(1)
        s_header.interface.change_to_english(logging=False)
        sleep(2)
        # s_header.interface.click_on_settings_when_start_language_is()

#fklklklkejlogggf
@catch_any_error_and_return_false
def after_scenario_change_laguage(scenario):
    if "Change language" in scenario.name:
        s_header.interface.change_to_english(logging=False)
        sleep(2)

# def close_popups():
#     print("in close_popups")
#     s_header.interface.stf_2.open_start_without_verification()
#     provoke_something()
#     dismiss = s_header.interface.dismiss_allow_to_share_installed_applications()
#     provoke_something()
#     interface = s_header.interface.skip_questionary_if_needed()
#     if interface and not dismiss:
#         provoke_something()
#         s_header.interface.dismiss_allow_to_share_installed_applications()
