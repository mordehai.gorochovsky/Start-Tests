# Created by itzik at 3/18/18
Feature: Permissions
# Enter feature description here
  Scenario: dismiss all permissions and allow all permissions
    Given android version higher or equal to 6.0
    When install start on the first time or clear cach if preloaded
    Then start video recording
    And wait for "2" seconds
    And open start without verification
    And wait for welcome button after installation up to "150" seconds
    And skip welcome if needed
    And skiping tutorial if needed
    And wait for "2" seconds
    Then dismiss al permission requests
    And wait for "3" seconds
    And swipe to multimedia hub
    And wait for "5" seconds
    Then verify request for access permission
    Then stop recording and save by name "dismiss_permissions"
    And wait for "20" seconds
    When install start on the first time or clear cach if preloaded
    Then start video recording
    And wait for "2" seconds
    And open start without verification
    And wait for welcome button after installation up to "150" seconds
    And skip welcome if needed
    And wait for "2" seconds
    Then allow all permissions
    And wait for "5" seconds
    And skip keep in touch screen
    And wait for "3" seconds
    And swipe to multimedia hub
    And wait for "5" seconds
    Then verify that camera was opened
    Then verify no permission request dispalyed for contacts and physical location
    Then verify no permission request for file access
    #And fail test


