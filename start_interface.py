import inspect
import time

from PIL import Image
from cv2 import cv2
from pathlib import Path
from subprocess import check_output

import os

import pytesseract

import config
import start_utilities as utl
import start_functions_2 as stf_2
from start_functions import StartFunctions, adb_functions, canvas, catch_any_error_and_return_false
from log_utilities import HtmlLog
from uiautomator_canvas import get_number_of_black_pixels, get_number_of_white_pixels
from features.environment import s_header

colorWidget = stf_2.WidgetColorUtilities()
phoneEvents = stf_2.PhoneEvents()
log_html = HtmlLog()
test = StartFunctions()
#
# @catch_any_error_and_return_false
# def click_on_Setup_Your_Wallpaper(theme)->bool:
#     log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
#     return test.select_item_in_settings_by_text(theme)
#
#
# def setup_wallpaper()->bool:
#     log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
#     return test.setup_your_wallpaper()

#
# def verify_in_Wallpapers_screen():
#     log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
#     adb_functions.screenshot_by_adb()
#     from_y = 0.04
#     to_y = 0.215
#     from_x = 0
#     to_x = 1
#     return test.verify_text_in_bounds(from_x, from_y, to_x, to_y, config.wallpaper)

#
# def verify_in_Choose_Your_Wallpaper_screen():
#     log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
#     title = test.get_screen_title()
#     if title == config.choose_your_wallpaper:
#         return True
#     else:
#         return False


def verify_in_Widget_Size_screen():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    title = test.get_screen_title()
    if title == config.WidgetSize:
        return True
    else:
        return False


def click_on_Select_Background():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.click_on_button_in_menu_screen("Select Background")


def click_on_abstract():
    print("in click_on_abstract")
    for abstract in config.abstract:
        if try_clicking_on_abstract(abstract):
            break


def try_clicking_on_abstract(abstrarct):
    print("in try_clicking_on_abstract")
    try:
        stf_2.device_obj(text=abstrarct).click()
        return True
    except Exception as e:
        print(e)
        return False


def get_to_abstract_themes_screen():
    print("in get_to_abstract_themes_screen")
    test.open_start(simple=True,logging=False)
    test.swipe_menu()
    test.click_on_plus_for_more_themes()
    stf_2.click_on_categories()
    click_on_abstract()
    time.sleep(6)


def set_black_background_via_more_themes():
    print("in set_black_background_via_more_themes")
    get_to_abstract_themes_screen()
    i = 0
    while i < 2:
        time.sleep(10)
        try:
            if test.try_to_set_black_background_via_more_themes():
                break
        except Exception as e:
            print (e)
        time.sleep(1)


def click_on_Setup_Your_Widgets(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.select_item_in_settings_by_text("Setup Your Widgets")


def click_on_Clock_Widget():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.select_item_in_Setup_Your_Widget(config.ClockWidget)
    #Todo add verefication
    return True


def click_on_Widget_Color(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.select_item_in_Setup_Your_Widget(config.WidgetColor)
    # return True


def click_on_Widget_Size(logging=True):
    print("in click_on_Widget_Size")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.select_item_in_Setup_Your_Widget(config.WidgetSize)


def set_widget_color_to_black(logging=False):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    colorWidget.swipe_to_black_region()
    colorWidget.click_ok_button()
    #Todo: add verification
    return True


def set_widget_color_to_black_and_verify():
    print("in set_widget_color_to_black_and_verify")
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:
        set_widget_color_to_black()
        press_back_n_times(3)
        time.sleep(1)
        black2 = get_number_of_black_pixels()
        #swipe_and_click_on_settings(logging=False)
        # swipe_left_by_menu_icon(logging=False)
        # test.click_on_setting_button(logging=False)
        go_to_setup_widgets_screen(logging=False)
        click_on_Widget_Color(logging=False)
        set_widget_color_to_white(logging=False)
        press_back_n_times(3)
        black1 = get_number_of_black_pixels()
        print("black widgets num of black pixels", black2, "white widgets number of black pixels ", black1)
        if black2 - black1 > 1000:
            return True
        else:
            return False
    except Exception as e:
        print(e)
        return None



# def multiple_screen_on_and_off(num):
#     log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
#     adb_functions.multiple_screen_on_and_off(num)


def set_widget_color_to_white(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    colorWidget.swipe_to_white_region()
    colorWidget.click_ok_button()
    # Todo: add verification
    return True


def verify_widget_changed(widget):
    # !!Attention!!: No decision logging needed
    test.get_to_start_home_screen()
    img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "widget_color_1.png")
    go_to_setup_your_widget_screen()
    flag = 0
    if widget == "color":
        click_on_Widget_Color()
        set_widget_color_to_white()
        flag = 1
    elif widget == "size":
        click_on_Widget_Size()
        stf_2.increase_decease_clock_widget(decrease=False)
        flag = 1
    if not flag:
        print("wrong widget name supplied")
        return False
    test.get_to_start_home_screen()
    img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "widget_color_2.png")
    result = canvas.compare_images(img1, img2)
    if result == "1" or not result:
        return False
    else:
        return True


def go_to_setup_your_widget_screen(logging=False):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    press_back_n_times(4)
    test.swipe_menu(logging=logging)
    test.click_on_setting_button(logging=logging)
    click_on_Setup_Your_Widgets(logging=logging)


def set_widget_color_to_default():
    go_to_setup_your_widget_screen()
    click_on_Widget_Color(logging=False)
    test.scroll_and_click_by_text("Defaults",logging=False)

#
# def set_black_wallpapper():
#     device_obj.press.back()
#     self.device_obj.press.back()
#     self.device_obj.press.back()
#     swipe_left_by_menu_icon()
#     self.swipe_menu()
#     self.click_on_setting_button()
#     self.select_item_in_settings_by_text(config.Settings_dict.setup_wallpaper)
#     try:
#         self.scroll_and_click_by_text(config.black_theme_name,False)
#     except Exception as e:
#         print(e)
#         print("second try with scrollable=True ")
#         self.scroll_and_click_by_text(config.black_theme_name)
#     os.environ["RAV_BLACK_WALLPAPPER"] = "1"
#
# def verify_widget_size_changed():
#     #!!Attention!!: No decision logging needed
#     test.get_to_start_home_screen()
#     img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "widget_size_1.png")
#     test.swipe_menu()
#     test.click_on_setting_button()
#     click_on_Setup_Your_Widgets()
#     click_on_Widget_Size()
#     stf2.increase_decease_widget(decrease = False)
#     #set_widget_color_to_white()
#     test.get_to_start_home_screen()
#     img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "widget_size_2.png")
#     result = canvas.compare_images(img1, img2)
#     if result == "1":
#         return False
#     else:
#         return True

def click_on_categories():
    stf_2.click_by_description("CATEGORIES")


def maximize_widget_size_and_verify():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return verify_widget_changed(widget="size")


def maximize_clock_widget_size(percent=50, logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return stf_2.increase_decease_clock_widget(decrease=False, num_of_clicks=percent)


def change_widget_color_to_white_and_verify():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return verify_widget_changed(widget="color")


def minimize_clock_widget(logging=True,percent=50):
    print("in minimize_clock_widget")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return stf_2.increase_decease_clock_widget(num_of_clicks=percent)


@catch_any_error_and_return_false
def minimize_date_widget(num_of_clicks=100):
    '''
    THIS FUNCTION NEEDS VERIFICATION - it has been  never executed in a test
    '''
    stf_2.decrease_widget_given_by_index(1,num_of_clicks)


def drag_widget_to_trash():
    print("in drag_widget_to_trash")
    w = test.screen_width
    h = test.screen_height
    x = int(0.5 * w)
    y = int(0.3 * h)
    x_e = x
    y_e = int(0.85 * h)
    test.device_obj.drag(x, y, x_e, y_e, steps=10)


def drag_widget_down(height_percent=0.6):
    w = test.screen_width
    h = test.screen_height
    x = int(0.5 * w)
    y = int(0.3 * h)
    x_e = x
    y_e = int(height_percent * h)
    test.device_obj.drag(x, y, x_e, y_e, steps=10)


def maximize_date_widget(num_of_clicks=100):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    reset_widget_size()
    time.sleep(1)
    stf_2.increase_widget_given_by_index(1,num_of_clicks)


@catch_any_error_and_return_false
def minimize_clock_widget_and_verify(num_of_clicks):
    print("in minimize_clock_widget_and_verify ")
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:
        minimize_clock_widget(logging=False, percent=num_of_clicks)
    except Exception as e:
        print(e)
        #verify_widget_was_minimised(num_of_clicks=num_of_clicks)
        return False
    white1, white2 = verify_widget_was_minimised(num_of_clicks)
    if white1 == False:
        return False
    print("clock minimized pixels ",white1,"clcok maximized pixels ",white2,"difference ",white1 - white2)
    if white2 - white1 > 300:
        return True
    else:
        return False


def verify_widget_was_minimised(num_of_clicks):
    print("in verify_widget_was_minimised")
    try:
        press_back_n_times(4)
        img = adb_functions.get_screen_image_by_name(name="minimized_clock.png")
        time.sleep(1)
        white1 = get_number_of_white_pixels(img)
        time.sleep(1)
        press_back_n_times(3)
        time.sleep(1)
        reset_widget_size()
        press_back_n_times(4)
        time.sleep(1)
        img2 = adb_functions.get_screen_image_by_name(name="maximized_clock.png")
        print("--1")
        time.sleep(1)
        white2 = get_number_of_white_pixels(img2)
        print("--2")
        return white1, white2
    except Exception as e:
        print(e)
        return False, False


def drag_widget_to_trash_and_verify(num_of_clicks):
    print("in verify_widget_was_minimised")
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:
        press_back_n_times(4)
        img = adb_functions.get_screen_image_by_name(name="date_widget_up.png")
        time.sleep(1)
        white1 = get_number_of_white_pixels(img)
        time.sleep(1)
        press_back_n_times(3)
        time.sleep(1)
        drag_widget_to_trash()
        time.sleep(1)
        img2 = adb_functions.get_screen_image_by_name(name="maximized_clock.png")
        print("--1")
        time.sleep(1)
        white2 = get_number_of_white_pixels(img2)
        print("--2")
        if white1 - white2 > 300:
            return True
        return False
    except Exception as e:
        print(e)
        return False


def drag_widget_and_verify(num_of_clicks):
    print("in verify_widget_was_minimised")
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:
        press_back_n_times(4)
        img = adb_functions.get_screen_image_by_name(name="date_widget_up.png")
        crop_img = adb_functions.crop_upper_screen(img)
        time.sleep(1)
        white1 = get_number_of_white_pixels(crop_img)
        time.sleep(1)
        press_back_n_times(3)
        time.sleep(1)
        drag_widget_down()
        time.sleep(1)
        img2 = adb_functions.get_screen_image_by_name(name="maximized_clock.png")
        crop_img2 = adb_functions.crop_upper_screen(img2)
        print("--1")
        time.sleep(1)
        white2 = get_number_of_white_pixels(crop_img2)
        print("--2")
        if white1 - white2 > 300:
            return True
        return False
    except Exception as e:
        print(e)
        return False


def go_to_widget_size():
    print("in widget size")

    swipe_and_click_on_settings(logging=False)
    test.scroll_and_click_by_text("Setup Your Widgets", logging=False)
    click_on_Widget_Size(logging=False)


def receive_call():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "verify_first.png")
    phoneEvents.call_to_device()
    time.sleep(6)
    img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "verify_last.png")
    result = canvas.compare_images(img1, img2)
    if result == "1" or not result:
        return False
    else:
        return True


def answer_the_call():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "verify_first.png")
    phoneEvents.answer_call()
    time.sleep(2)
    img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "verify_last.png")
    result = canvas.compare_images(img1, img2)
    if result == "1" or not result:
        return False
    else:
        return True

@catch_any_error_and_return_false
def multiple_clicks_on_ring(num_of_clicks,verification=True,behave=False):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3],behave)
    test.device_obj.screen.on()
    #img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "multiple_clicks_ring_1.png")
    for _ in range(int(num_of_clicks)):
        test.click_on_main_ring()
    if verification:
        return test.verify_in_start_home_screen_by_elements()
        # time.sleep(3)
        # test.device_obj.screen.on()
        # img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "multiple_clicks_ring_2.png")
        # result = canvas.compare_images(img1, img2, 8)
        # if result == "1":
        #     return True
        # return False


def multiple_screen_on_and_off(number_of_circles,verification=True,behave=False):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3], behave)
    test.device_obj.screen.on()
    if verification:
        img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                               "multiple_screen_off_on_1.png")
    for _ in range(int(number_of_circles)):
        test.screen_off()
        time.sleep(0.5)
        test.screen_on(logging=False)
        time.sleep(0.5)
    if verification:
        time.sleep(7)
        test.device_obj.screen.on()
        img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                               "multiple_screen_off_on_2.png")
        if verification:
            result = canvas.compare_images(img1, img2, 8)
            if result == "1" :
                return True
            else:
                return test.verify_in_device_home_screen_by_elements()
    #return False


def press_back_n_times(n):
    test.back(n)

@catch_any_error_and_return_false
def multiple_press_back(num_off_clicks, img1=None, pref="",verification=True,behave=False,logging=True):
    print("in multiple_press_back")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3],behave)
    if not img1:
        test.device_obj.screen.on()
        time.sleep(1)
        if verification:
            img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                                   "multiple_press_back_{}_1.png".format(pref))
            time.sleep(5)
    for _ in range(int(num_off_clicks)):
        press_back()
    time.sleep(4)
    test.device_obj.screen.on()
    if verification:
        img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                               "multiple_press_back_{}_2.png".format(pref))
        result = canvas.compare_images(img1, img2, 8)
        if result == "1":
            return True
        else:
            return False
    else:
        return True


def verify_in_any_start_screen(timeout):
    print("in verify_in_any_start_screen")
    res = stf_2.verify_element_exists_by_resource_id("com.celltick.lockscreen:id/background_container",timeout)
    print("element exists? ", res)


def verify_in_start_screen_after_instal(timeout):
    print("in verify_in_start_screen_after_instal")
    while timeout > -19:
        try:
            res0 = test.open_start_simple()
            if res0:
                return res0
        except Exception as e:
            print (e)
        time.sleep(15)
        timeout -= 20


def back_from_drawer(img,behave=False):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3], behave)
    res = multiple_press_back(1, img1=img, pref="drawer")
    if res:
        # img2 = Path(test.device_folder) / "multiple_press_back_{}_2.png".format("drawer")
        # res2 = test.verify_in_start_home_screen_by_lock_png(img2)
        res2 = test.verify_in_start_home_screen_by_elements()
        return res2
    return not res


# def back_from_settings_screen(behave=False):
#     log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3],behave)
#     res = multiple_press_back(1, pref="settings")
#     if res:
#        # img2 = Path(test.device_folder) / "multiple_press_back_{}_2.png".format("settings")
#         res2 = test.verify_in_start_home_screen_by_elements()#test.verify_in_start_home_screen_by_lock_png(img2)
#         return res2
#     return not res


def back_from_security_screen(behave=False):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3], behave)
    try:
        res = multiple_press_back(1, pref="security")
        if res:
            # img2 = Path(test.device_folder) / "multiple_press_back_{}_2.png".format("security")
            #res2 = test.verify_in_start_home_screen_by_lock_png(img2)
            res2 = test.verify_in_start_home_screen_by_elements()
            return res2
        return not res
    except Exception as e:
        print(e)
        return False

@catch_any_error_and_return_false
def multiple_press_home(num_off_clicks,verification=True,behave=False):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3],behave)
    # test.device_obj.screen.on()
    # stf_2.press_back()
    time.sleep(2)
    if verification:
        img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                               "multiple_press_home_1.png")
    for _ in range(int(num_off_clicks)):
        stf_2.press_home()
        test.device_obj.screen.on()
    if verification:
        img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                           "multiple_press_home_2.png")
        result = canvas.compare_images(img1, img2, 8)
        if result == "1":
            return True
        return False
    # else:
    #     return not test.verify_in_start_home_screen_by_lock_png(img2)


@catch_any_error_and_return_false
def multiple_click_on_settings_icon(num_off_clicks, verification=True):
    print("in multiple_click_on_settings_icon")
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    (w, h) = adb_functions.get_physical_device_screen_size()
    (x, y) = stf_2.get_menu_true_menu_point(w, h)
    test.device_obj.screen.on()
    press_back()
    time.sleep(2)
    if verification:
        img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                               "multiple_click_on_settings_1.png")
    time.sleep(5)

    time.sleep(3)
    for _ in range(int(num_off_clicks)):
        stf_2.device_obj.click(x, y)
    time.sleep(2)
    test.device_obj.screen.on()
    if verification:
        img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                               "multiple_click_on_settings_2.png")  # TODO: fix typo
        result = canvas.compare_images(img1, img2, 8)
        if result == "1":
            return True
        else:
            return test.verify_in_start_home_screen_by_elements()


def open_upper_drawer(behave):
    print("in open_upper_drawer")
    try:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3], behave)
        w = test.screen_width
        h = test.screen_height
        (x, y) = stf_2.get_upper_starter_point(w, h)
        test.device_obj.screen.on()
        img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                               "verify_first.png")
        result = swipe_drawer_and_verify(img1, w, x, y)
        if result == "1" or not result:
            print("swipe drawer failed trying once again")
            result = swipe_drawer_and_verify(img1, w, x, y)
        if result == "1" or not result:
            return False
        else:
            return True
    except Exception as e:
        print(e)
        return False


def open_second_drawer(behave):
    print("in open_upper_drawer")
    try:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3], behave)
        w = test.screen_width
        h = test.screen_height
        (x, y) = stf_2.get_second_starter(w, h)
        test.device_obj.screen.on()
        img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                               "verify_first.png")
        result = swipe_drawer_and_verify(img1, w, x, y)
        if result == "1" or not result:
            print("swipe drawer failed trying once again")
            result = swipe_drawer_and_verify(img1, w, x, y)
        if result == "1" or not result:
            return False
        else:
            return True
    except Exception as e:
        print(e)
        return False


def swipe_drawer_and_verify(img1, w, x, y):
    print("in swipe_drawer_and_verify")
    stf_2.device_obj.swipe(x, y, int(w * 0.85), y, steps=200)
    time.sleep(2)
    test.device_obj.screen.on()
    img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) /
                                           "verify_last.png")
    result = canvas.compare_images(img1, img2, allowed_dif=20)
    return result


def swipe_left_by_menu_icon(behave = False,logging=True) -> bool:
    print("in swipe_left_by_menu_icon")
    try:
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3],behave)
        # (w, h) = adb_functions.get_physical_device_screen_size()
        # (x, y) = stf_2.get_menu_true_menu_point(w, h)
        # time.sleep(3)
        # stf_2.device_obj.swipe(x, y, int(x*0.2), y)
        # time.sleep(2)
        # res = bool(test.get_settings_button_coordinates())
        # if not res:
        #     x = int(w * 0.9)
        #     y = int(y * 0.5)
        #     stf_2.device_obj.swipe(x, y, int(x * 0.2), y)
        #     time.sleep(3)
        test.device_obj.screen.on()
        time.sleep(1)
        test.device_obj().swipe.left()
        time.sleep(2)
        res = bool(test.get_settings_button_coordinates())
    except Exception as e:
        print(e)
        return False
    return res


def swipe_left_without_verification(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.device_obj().swipe.left()


def click_on_security(verification = True):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:
        security_crd = test.get_settings_button_coordinates(text="Security")
        stf_2.device_obj.click(security_crd[0], security_crd[1])
        time.sleep(2)
        #r_id = "android:id/title"
        if verification:
            return verify_in_security_screen()
    except Exception as e:
        print(e)
        return False


def verify_in_security_screen():
    if stf_2.verify_security_screen_not_rooted():
        return True
    else:
        return stf_2.verify_security_screen_rooted()


def disable_widget(widget):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    stf_2.disable_widget(widget)


def enable_widget(widget):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.select_item_in_Setup_Your_Widget(widget)


def disable_clock_widget(logging=False):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    clock_w = "Clock Widget"
    stf_2.disable_widget(clock_w)


def disable_date_widget():
    date_w = "Date Widget"
    stf_2.disable_widget(date_w)


def disable_battery_meter_widgget():
    text = "Battery Meter Widget"
    stf_2.disable_widget(text)


def enable_clock_widget():
    print("in enable_clock_widget")
    clock_w = "Clock Widget"
    stf_2.enable_widget(clock_w)


def enable_date_widget():
    date_w = "Date Widget"
    stf_2.enable_widget(date_w)

#
# def clik_on_widget(widget):
#     log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
#     try:
#         stf_2.click_on_widget(widget)
#         return True
#     except Exception as e:
#         print(e)
#         return False

def verify_default_widget_alignment():
    try:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        go_to_setup_widgets_screen()
        print("33333333333333")
        test.select_item_in_Setup_Your_Widget(config.WidgetAlignmemt)
        print("444444444444444")
        return stf_2.widget_alignment_click_on_default()
        # test.scroll_and_click_by_text("Wallpaper Default")

    except Exception as e:
        print("Caught Error ", e)
        return False


def go_to_setup_widgets_screen(logging=False):
    print("in go_to_setup_widgets_screen")
    press_back_n_times(4)
    test.swipe_menu(logging=logging)
    time.sleep(2)
    test.click_on_setting_button(logging=logging)
    time.sleep(2)
    click_on_Setup_Your_Widgets(logging=logging)
    time.sleep(2)


def move_clock_widget() -> bool:
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    (w, h) = adb_functions.get_physical_device_screen_size()
    (x, y) = stf_2.get_clock_widget_coordinates(w, h)
    print("clock coordinates x={} y={}".format(x, y))
    test.device_obj.screen.on()
    img1 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "verify_first.png")
    time.sleep(3)
    stf_2.long_swipe_on_point_to_point(pt_to=(x, int(h*0.4)), pt_on=(x, y))
    img2 = adb_functions.screenshot_by_adb(Path(test.device_folder) / "verify_last.png")
    result = canvas.compare_images(img1, img2)
    return result == "1"


def bring_clock_widget_to_initial_state():
    press_back()
    press_back()
    press_back()
    test.swipe_menu()
    test.click_on_setting_button()
    click_on_Setup_Your_Widgets()
    click_on_Clock_Widget()
    # Todo: add verification ??
    return True


@catch_any_error_and_return_false
def swipe_and_click_on_settings(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    res = False
    try:
        test.swipe_menu(logging=logging)
    except Exception as e:
        print(e)
    try:
        res = test.click_on_setting_button(logging=logging)
    except Exception as e:
        print(e)
        press_on_settings_by_index()
        res = True
    return res


def fix_settings_issue():
    press_back()
    press_back()
    press_back()
    swipe_and_click_on_settings()
    press_back()
    press_back()
    press_back()


def skip_tutorial():
    print("in skip_tutorial")
    test.swipe_menu()
    test.click_on_setting_button()
    test.scroll_and_click_by_text("Tutorial")
    skip_tutorial_if_needed()


def skip_tutorial_if_needed(logging=False):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:
        set_screen_state_on()
        skip_keep_in_touch_screen_if_needed()
        txt = test.device_obj(resourceId="com.celltick.lockscreen:id/tutorial_1_title").text
        if ("new to" in txt.lower()):
            test.skip_tutorial_by_ok()
        skip_keep_in_touch_screen_if_needed()
    except Exception as e:
        print(e)


@catch_any_error_and_return_false
def skip_tutorial_by_ok():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.skip_tutorial_by_ok()


def skip_keep_in_touch_screen_if_needed(logging=True):
    print("in skip_keep_in_touch_screen_if_needed")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:

        if stf_2.verify_element_exists_by_resource_id("com.celltick.lockscreen:id/user_mail", 10):
            print("inside if - cliccking on skip")
            test.device_obj(resourceId="com.celltick.lockscreen:id/skip_btn").click()
            print("returning true")
            return True
        print("returning false")
        return False
    except Exception as e:
        print(e)
        print("in exceeeeeptiooon!")
        return None


#
# def skip_questionary():
#     stf_2.skip_questionary()


def skip_rating():
    stf_2.skip_rating()

# def click_on_security(behave=False,logging=False):
#     log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3], behave)
#     return test.click_on_setting_button(logging=False,menu="Security",behave=behave)

def press_back(logging=False,behave=False):
    #print("in press_back")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3], behave)
    stf_2.device_obj.press.back()


def click_on_contact_us():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.scroll_and_click_by_text("Contact Us")

@catch_any_error_and_return_false
def verify_in_contact_us_screen():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.verify_in_contact_us_screen_3_options()

@catch_any_error_and_return_false
def verify_tutorial_opened():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.verify_tutorial_opened()


def verify_disable_start_opened():
    try:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        title = test.device_obj(resourceId="com.celltick.lockscreen:id/title").text
        return title.lower() == "Disable Start?".lower()
    except Exception as e:
        print(e)

@catch_any_error_and_return_false
def click_on_disable_start_choice_button():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.device_obj(resourceId="com.celltick.lockscreen:id/disable_start_btn").click()

@catch_any_error_and_return_false
def verify_disable_start_periods_apppear():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.verify_disable_periods_appear()

@catch_any_error_and_return_false
def click_on_ok_buttton_in_disable_start_for_period_screen():
    print("in click_on_ok_buttton_in_disable_start_for_period_screen")
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.device_obj(resourceId="com.celltick.lockscreen:id/ok_btn").click()


@catch_any_error_and_return_false
def set_screen_state_on(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.set_screen_state(1,logging=False)


@catch_any_error_and_return_false
def set_screen_state_off(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.set_screen_state(0,logging=False)


@catch_any_error_and_return_false
def press_on_home_button(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    stf_2.press_home()


@catch_any_error_and_return_false
def verify_android_version_higher_or_equel_to_6():
    print("in verify_android_version_higher_or_equel_to_6")
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    version = test.adb.get_device_android_version()
    version1 = int(version.split(".")[0])
    print("android version ",version)
    if version1 >= 6:
        return True
    print("android version lower then 6.0")
    return False


def start_reinstall_with_cache_delete():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.adb.uninstall_and_install_for_displayed_permission()


def skip_welcome_if_needed(logging = True):
    print("in skip_welcome_if_needed")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:
        if len(test.device_obj(resourceId="com.celltick.lockscreen:id/welcome_start_button")) > 0:
            print("skiping welcome message")
            test.device_obj(resourceId="com.celltick.lockscreen:id/welcome_start_button").click()
    except Exception as e:
        print(e)


@catch_any_error_and_return_false
def allow_permissions_on_start_first_time_install(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    #skip_welcome_if_needed(logging = False)
    return test.allow_permissions_if_needed()


@catch_any_error_and_return_false
def dismiss_permission_requests():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return test.deny_permission()

def open_start():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.device_obj.screen.on()
    time.sleep(1)
    return test.open_start_simple(logging=False)

#
# def open_start_first_time():
#     log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
#     stf_2.open_start_without_verification()
#     time.sleep(15)


def open_tutorial_and_learn_it():
    test.swipe_menu(logging=False)
    test.click_on_setting_button(logging=False)
    test.scroll_and_click_by_text("Tutorial")
    test.skip_tutorial_by_ok()


def verify_downloads_manager_installed():
    cmd1 = "shell monkey -p com.android.providers.downloads.ui -c android.intent.category.LAUNCHER 1"
    cmd2 = "shell monkey -p com.android.documentsui -c android.intent.category.LAUNCHER 1"
    cmd3 = "shell monkey -p com.balthapps.downloadmanager -c android.intent.category.LAUNCHER 1"


@catch_any_error_and_return_false
def skip_questionary_if_needed(logging=True):
    print("in skip_questionary_if_needed")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return utl.if_questionary_skip()


def dismiss_allow_to_share_installed_applications(logging=False):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:
        test.device_obj(resourceId="com.celltick.lockscreen:id/user_consent_dismiss").click()
        os.environ["RAV_SHARE_APPS_LIST_DISMISSED"] = "true"
        return True
    except Exception as e:
        print(e)
        return False


def open_start_without_verification():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    stf_2.open_start_without_verification()

@catch_any_error_and_return_false
def open_start_first_time(logging=False):
    print("in open_start_first_time")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    check_output("adb -s {} shell am start com.celltick.lockscreen".format(test.device).split())
    time.sleep(20)
    skip_welcome_if_needed(logging=False)
    skip_questionary_if_needed(logging=False)
    test.allow_permissions_if_needed()
    skip_keep_in_touch_screen_if_needed(logging=False)
    # dismiss_allow_to_share_installed_applications()
    return test.verify_in_start_home_screen_by_elements()

@catch_any_error_and_return_false
def fail_test_to_see_the_video():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in fail_test_to_see_the_video")
    return False


def open_camera_by_swiping_to_multimedia_hub(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    press_back_n_times(4)
    try:
        camera = test.multimedia_hub_point
        if not camera:
            camera = test.guess_multimedia_hub_point()
        lock = test.lock_point
        if not lock:
            lock = test.guess_lock_point()
        test.device_obj.swipePoints([lock,camera],100)
        return True
    except Exception as e:
        print(e)
        return False


def verify_request_for_camera_permissions():
    open_camera_by_swiping_to_multimedia_hub(logging=False)
    permission_request = verify_request_for_access_permission(logging=False)
    if permission_request:
        allow_all_permisions()
        return True
    return False


def verify_request_for_access_permission(logging=True):
    print("in verify_request_for_access_permission")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:
        txt = test.device_obj(resourceId="com.android.packageinstaller:id/permission_message").text
    except Exception as e:
        print(e)
        return False
    res = "Allow".lower() in txt.lower()
    print("permission request displayed? {}",res)
    return res


@catch_any_error_and_return_false
def verify_no_request_for_access_permission(logging=True):
    print("in verify_no_request_for_access_permission")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    return not verify_request_for_access_permission(logging=False)


def verify_camera_by_elements():
    try:
        for rid in config.camera_ids:
            if len(stf_2.device_obj(resourceId=rid)):
                return True
    except Exception as e:
        print(e)
        return False


def click_in_the_middle_of_screen():
    print("in click_in_the_middle_of_screen")
    try:
        width = test.screen_width
        height = test.screen_height
        if not width or not height:
            return
        test.device_obj.click(int(width/2),int(height/2))
        time.sleep(1)
    except Exception as e:
        print(e)


@catch_any_error_and_return_false
def verify_camera_opened(logging=True):
    print("in verify_camera_opened")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    activity = test.adb.get_recent_activity()
    print("activity: ",activity)
    if not "camera" in str(activity).lower():
        print("no camera activity")
        allow_all_permisions()
        return False
    ''' not returnung True in "else" because camera shoud be verified from screen'''
    # else:
    #     return True
    time.sleep(2)
    res = verify_camera_no_logging()
    if not res:
        click_in_the_middle_of_screen()
        time.sleep(3)
    return verify_camera_no_logging()


def verify_camera_no_logging():
    print("in verify_camera_no_logging")
    r_id = "android:id/decor_content_parent"
    try:
        camera = len(stf_2.device_obj(resourceId="android:id/decor_content_parent"))
        if camera:
            return True
        res = verify_camera_by_elements()
        if not res:
            allow_all_permisions()
        return res
    except Exception as e:
        print(e)
        return False


@catch_any_error_and_return_false
def wait_for_welcome_button_after_install(sec):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    welcome_id= "com.celltick.lockscreen:id/welcome_start_button"
    stf_2.verify_element_exists_by_resource_id(welcome_id,sec)


def verify_clock_displayed(logging=False):
    print("in verify_clock_displayed")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    try:
        path = Path(test.adb.device_folder) / "clock_present.png"
        path2 = Path(test.adb.device_folder) / "clock_present_cropped.png"
        path3 = Path(test.adb.device_folder) / "clock_present_cropped_resized.png"
        path4 = Path(test.adb.device_folder) / "clock_present_cropped_resized_last.png"
        path5 = Path(test.adb.device_folder) / "clock_present_cropped_resized_last_one.png"
        print("path ",path)
        test.adb.screenshot_by_adb(path)
        print("aaaaa")
        time.sleep(1)
        img = cv2.imread(str(path))
        print("bbbbb")
        time.sleep(1)
        crop_img = adb_functions.crop_clock_widget(img)
        time.sleep(1)
        cv2.imwrite(str(path2),crop_img)
        time.sleep(2)
        print("3")

        crop_img2, text = extract_text(path2)
        time.sleep(1)
        res1 = stf_2.verify_clock_displayed_by_extracted_text(text)
        if res1:
            return True
        #crop_img2 = cv2.imread("")
        # crop_img2 = Image.open("")
        print("4")
        time.sleep(1)
        res, crop_img2 = resize_and_extract_text(path3, 0.5)
        print("")
        print("5")
        if not res:
            print(6)
            time.sleep(1)
            res, crop_img2 = resize_and_extract_text(path4, 0.5)
            if not res:
                print(7)
                time.sleep(1)
                res, crop_img2 = resize_and_extract_text(path5, 0.5)
        return res
    except Exception as e:
        print("error: ",e)
        return None


@catch_any_error_and_return_false
def resize_and_extract_text(path3, ratio):
    print("in resize_and_extract_text")
    print("path ",path3)
    #print("image type: ",type(crop_img2))
    #cv2.imwrite(path3,crop_img2)
    print("a")
    crop_img2 = cv2.imread(str(path3))
    print("b")
    path3 = resize_image_by_ratio(crop_img2, path3, ratio)
    print("c")
    crop_img2, result = extract_text(path3)
    print("d")
    res = stf_2.verify_clock_displayed_by_extracted_text(result)
    return res, crop_img2


@catch_any_error_and_return_false
def extract_text(path3):
    print("in extract_text")
    crop_img2 = Image.open(path3)
    result = pytesseract.image_to_string(crop_img2)
    print("extracted text ", result)
    return crop_img2, result


@catch_any_error_and_return_false
def resize_image_by_ratio(crop_img2, path, ratio):
    print("in resize_image_by_ratio")
    img_scaled = cv2.resize(crop_img2, None, fx=ratio, fy=ratio, interpolation=cv2.INTER_LINEAR)
    print("finished rescaling")
    cv2.imwrite(str(path), img_scaled)
    return path


@catch_any_error_and_return_false
def disable_clock_widget_if_enabled():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    clock_dispalyed = verify_clock_displayed()
    if clock_dispalyed:
        print("clock  displayed - disabling now")
        go_to_setup_widgets_screen()
        disable_clock_widget(logging=False)


@catch_any_error_and_return_false
def enable_clock_widget_if_disabled():
    print("in enable_clock_widget_if_disabled")
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    clock_dispalyed = verify_clock_displayed()
    if not clock_dispalyed:
        print("clock  displayed - disabling now")
        go_to_setup_widgets_screen()
        enable_clock_widget()


def stop_recording_and_save_by_name(name):
    s_header.stop_recording(save_name=name)


def unlock_device():
    try:
        test.swipe_screen()
        stf_2.device_obj.press.home()
    except Exception as e:
        print(e)


def reset_google_ads_id():
    print("in reset_google_ads_id")
    device_id =  stf_2.device_id
    open_google_settings = "shell am start -n com.google.android.gms/com.google.android.gms.app.settings.GoogleSettingsActivity".format(device_id)
    open_settings = "shell am start -a android.settings.SETTINGS".format(device_id)
    press_on_home_button(logging=False)
    press_on_home_button(logging=False)
    press_back_n_times(3)
    try:
        adb_functions.adb(open_settings)
        if test.scroll_and_click_by_text("Google"):
            time.sleep(2)
            return press_on_ads_and_reset_id()
        else:
            press_back_n_times(2)
            res = adb_functions.adb(open_google_settings)
            if "error" in str(res).lower():
                print("error while opening Google Settings")
                return False
            return press_on_ads_and_reset_id()
    except Exception as e:
        print(e)
        return False


def press_on_ads_and_reset_id():
    print("in press_on_ads_and_reset_id")
    try:
        if test.scroll_and_click_by_text("Ads"):
            time.sleep(2)
            if test.scroll_and_click_by_text("Reset advertising ID"):
                time.sleep(2)
                if test.scroll_and_click_by_text("OK"):
                    return True
    except Exception as e:
        print(e)
        return False


def widgets_alignment_center():
    press_back_n_times(4)
    swipe_and_click_on_settings(logging=False)
    try:
        test.scroll_and_click_by_text("Setup Your Widgets")
        test.scroll_and_click_by_text("Widget Alignment")
        test.scroll_and_click_by_text("Center on Screen")
    except Exception as e:
        print(e)
    press_back_n_times(3)


def guess_search_hub():
    screen_height = test.screen_height
    screen_width = test.screen_width
    y = int(screen_height * 1.04)
    x = int(screen_width * 0.5)
    return x,y


def go_to_search_hub():
    press_back_n_times(4)
    lock = test.lock_point
    if not lock:
        lock = test.guess_lock_point()
    search_hub = guess_search_hub()
    test.device_obj.swipe(lock[0],lock[1],search_hub[0],search_hub[1],200)


def go_to_select_backgound(logging):
    press_back_n_times(4)
    swipe_left_by_menu_icon(logging=logging)
    time.sleep(2)
    test.scroll_and_click_by_text("Select Background")


def allow_all_permisions():
    print("in allow_all_permisions")
    try:
        res = test.allow_permission()
        while res:
            res = test.allow_permission()
    except Exception as e:
        print(e)


@catch_any_error_and_return_false
def verify_no_permission_request_displayed_for_contacts_and_physical_location(logging=True):
    print("in verify_no_permission_request_displayed_for_contacts_and_physical_location")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    press_back_n_times(4)
    go_to_search_hub()
    time.sleep(2)
    no_permission_request = verify_no_request_for_access_permission(logging=False)
    if not no_permission_request:
        allow_all_permisions()
        time.sleep(3)
        return False
    return True


@catch_any_error_and_return_false
def verify_no_permission_request_for_file_access(logging=True):
    print("in verify_no_permission_request_for_file_access")
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    press_back_n_times(4)
    go_to_select_backgound(logging=False)
    time.sleep(2)
    no_permission_request = verify_no_request_for_access_permission(logging=False)
    if not no_permission_request:
        allow_all_permisions()
        time.sleep(2)
        press_back_n_times(2)
        return False
    return True


def reset_widget_size():
    print("in reset_widget_size")
    press_back_n_times(4)
    time.sleep(1)
    go_to_widget_size()
    time.sleep(1)
    reset_id = "com.celltick.lockscreen:id/button_reset_id"
    try:
        test.device_obj(resourceId=reset_id).click()
    except Exception as e:
        print("error caught")
        print(e)
        test.scroll_and_click_by_text("Reset")


def click_on_widget_alignment(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.scroll_and_click_by_text("Widget Alignment")


def change_to_russian():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to russian")
    adb_functions.change_language(test.device, config.languages["Russian"][0])


def change_to_arabic():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to arabic")
    adb_functions.change_language(test.device, config.languages["Arabic"][0])


def change_to_chinese():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to chinese")
    adb_functions.change_language(test.device, config.languages["Chinese"][0])


def change_to_spanish():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to spanish")
    adb_functions.change_language(test.device, config.languages["Spanish"][0])


def change_to_portuguese():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to Portuguese")
    adb_functions.change_language(test.device, config.languages["Portuguese"][0])


def change_to_french():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to French")
    adb_functions.change_language(test.device, config.languages["French"][0])


def change_to_german():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to German")
    adb_functions.change_language(test.device, config.languages["German"][0])


def change_to_japanese():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to Japanese")
    adb_functions.change_language(test.device, config.languages["Japanese"][0])


def change_to_italian():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to Italian")
    adb_functions.change_language(test.device, config.languages["Italian"][0])


def change_to_english(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to English")
    adb_functions.change_language(test.device, config.languages["English"][0])


def change_to_hebrew():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in changing to Hebrew")
    adb_functions.change_language(test.device, config.languages["Hebrew"][0])

@catch_any_error_and_return_false
def change_language_to(language):
    print("in change_language_to")
    if language == "Hebrew":
        change_to_hebrew()
    elif language == "Spanish":
        change_to_spanish()
    elif language == "German":
        change_to_german()
    elif language == "Russian":
        change_to_russian()
    elif language == "French":
        change_to_french()
    elif language == "Italian":
        change_to_italian()
    elif language == "Arabic":
        change_to_arabic()
    elif language == "Japanese":
        change_to_japanese()
    elif language == "English":
        change_to_english()
    elif language == "Portuguese":
        change_to_portuguese()
    elif language == "Chinese":
        change_to_chinese()
    else:
        print("{} language is not supported".format(language))
        return False
    return True


@catch_any_error_and_return_false
def click_on_settings_when_start_language_is(language):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    settings = config.languages[language][1]
    test.device_obj(text=settings).click()

def verify_language(language):
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    print("in verify_language")
    print("language to verify:  ",language)
    settings = None
    try:
        print("a1")
        press_back_n_times(4)
        time.sleep(2)
        test.swipe_screen()
        time.sleep(2)
        try:
            print("a2")
            settings = config.languages[language][1]
            print("99")
        except Exception as e:
            print("a3")
            print("has an exception")
            print(e)
            print("wrong language name or language is not supported yet")
            return False
        try:
            print("a4")
            if len(test.device_obj(text=settings)):
                print("a5")
                print("language verified")
                return True
        except Exception as e:
            print("a6")
            print("in exception")
            print(e)
            return False
        print("a7")
    except Exception as e:
        print("a8")
        print(e)
        return False


def go_to_languages():
    print("in go_to_languages")
    try:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        press_back_n_times(3)
        print("1")
        test.swipe_screen()
        print("2")
        test.click_on_setting_button(logging=False)
        print("3")
        test.scroll_and_click_by_text(text="Languages",logging=False)
        print("out of go_to_languages")
    except Exception as e:
        print(e)


@catch_any_error_and_return_false
def verify_in_languages_screen():
    id = "com.celltick.lockscreen:id/styled_textview"
    if "languages" in str(test.device_obj(resourceId=id).text).lower():
        return  True
    return False


def change_start_language_to_english(logging=True):
    if logging:
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.scroll_and_click_by_text(text=config.settings_languages["English"],logging=False)


def change_start_language_to_spanish():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.scroll_and_click_by_text(text=config.settings_languages["Spanish"],logging=False)


def change_start_language_to_russian():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.scroll_and_click_by_text(text=config.settings_languages["Rusian"],logging=False)


def change_start_language_to_hebrew():
    test.scroll_and_click_by_text(text=config.settings_languages["Hebrew"],logging=False)


def change_start_language_to_japanese():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.scroll_and_click_by_text(text=config.settings_languages["Japanese"],logging=False)


def change_start_language_to_portuguese():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.scroll_and_click_by_text(text=config.settings_languages["Portuguese"],logging=False)


def change_start_language_to_italian():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.scroll_and_click_by_text(text=config.settings_languages["Italian"],logging=False)


def change_start_language_to_german():
    test.scroll_and_click_by_text(text=config.settings_languages["German"],logging=False)


def change_start_language_to_arabic():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.scroll_and_click_by_text(text=config.settings_languages["Arabic"],logging=False)


def change_start_language_to_french():
    log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    test.scroll_and_click_by_text(text=config.settings_languages["French"],logging=False)


def change_start_language_to(language):
    print("in change_language_to")
    if language == "Hebrew":
        change_start_language_to_hebrew()
    elif language == "Spanish":
        change_start_language_to_spanish()
    elif language == "German":
        change_start_language_to_german()
    elif language == "Russian":
        change_start_language_to_russian()
    elif language == "French":
        change_start_language_to_french()
    elif language == "Italian":
        change_start_language_to_italian()
    elif language == "Arabic":
        change_start_language_to_arabic()
    elif language == "Japanese":
        change_start_language_to_japanese()
    elif language == "English":
        change_start_language_to_english()
    elif language == "Portuguese":
        change_start_language_to_portuguese()
    else:
        print("+" * 30)
        print("{} language is not supported".format(language))
        print("+" * 30)
        return False
    return True


def press_on_settings_by_index():
    res = test.device_obj(resourceId="com.celltick.lockscreen:id/customization_prefs_label_id")
    for ind, item in enumerate(res):
        print(item.text)
        print(ind)
        if ind == 1:
            item.click()
            return


def click_on_languages_by_index():
    test.device_obj(scrollable=True).scroll.toBeginning(steps=100, max_swipes=1000)
    class_ = "android.widget.RelativeLayout"
    arr = test.device_obj(className=class_)
    for  ind ,item in enumerate(arr):
        if ind == 0:
            item.click()
            return


def disable_full_screen():
    try:
        go_to_advanced_settings()
        test.scroll_on_advanced_settings_list_and_check("Full Screen", False)
        time.sleep(1)
    except Exception as e:
        print(e)


def go_to_advanced_settings():
    press_back_n_times(4)
    time.sleep(1)
    swipe_left_without_verification(logging=False)
    time.sleep(1)
    test.click_on_setting_button(logging=False)
    time.sleep(1)
    test.click_on_advanced_settings(logging=False)
    time.sleep(1)
