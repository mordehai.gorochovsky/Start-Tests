import pytesseract as pytesseract
from PIL import Image
import settings as set
#from uiautomator_canvas import Point


def get_text_from_image( img, search_text):
    # Recognize text with tesseract for python
    result = pytesseract.image_to_string(Image.open(img))
    if search_text in result:
        print(result)
        return True
    return False



def long_swipe_on_point_from_point(pt_from,pt_on):
    # print("pt_ftrom", pt_from)
    # print("pt_on", pt_on)
    # print("pt_from type {}".format(type(pt_from)))
    # print("pt_on type {}".format(type(pt_on)))
    point_from = (pt_from.x,pt_from.y)
    point_on_1= (pt_on.x, pt_on.y)
    point_on_2= (pt_on.x+1,pt_on.y+1)
    #point_on_2 = (point_on_2.x,point_on_2.y)
    #point_on_2 = (point_on_1[0] + 1, point_on_1[1] + 1)
    points_array = [point_from]
    flag = 0
    for i in range(200):
        if flag == 0:
            points_array.append(point_on_1)
            flag = 1
        else:
            points_array.append(point_on_2)
            flag = 0
    points_array.append(point_from)
    set.device_obj.swipePoints(points_array, steps=2)

